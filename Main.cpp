#include "Main.h"

#include "b_lib/Dynamic_Array.h"
#include "b_lib/File.h"
#include "b_lib/Threading.h"
#include "b_lib/Font.h"

#include "UI.h"



#include <locale.h>
#include <io.h>
#include <fcntl.h>
#include <pathcch.h> //For PathCchRemoveFileSpec

#include <cctype>





void convert_windows_path_separators_to_normal_ones(Unicode_String* str)
{
	for (int i = 0; i < str->length; i++)
	{
		char32_t c = str->data[i];

		if (c == U'\\')
		{
			str->data[i] = U'/';
		}
	}
}





void Typer::init()
{
	ZoneScoped;

	{
		Time_Measurer tm = create_time_measurer();
		Reflection::init();
		log(ctx.logger, U"Reflection::init() took % ms", tm.ms_elapsed_double());
	}

	{
		log(ctx.logger, U"----");
		log(ctx.logger, U"sizeof(Typer)         = %", sizeof(Typer));
		log(ctx.logger, U"----");
	}

	fonts = Dynamic_Array<Font>(16, c_allocator);
	time_measurer = create_time_measurer();



	nodes = make_array<Node>(32, c_allocator);


	create_arena_allocator(&frame_allocator, c_allocator, 12 * 1024 * 1024);
#ifdef ALLOCATOR_NAMES
	frame_allocator.name = "frame_allocator";
#endif
	defer{ frame_allocator.reset(); };
	
	


	auto iter = iterate_files(U"fonts", c_allocator);
	while (iter.next().is_not_empty())
	{
		ZoneScopedN("load_font");

		// Make sure to supply full font paths, because working directory of this process gets changed a lot,
		//    and if font is not loaded yet, local directory will to point to non-existing directory.
		// @TODO: cleanup (fix)
		load_font(path_concat(c_allocator, Unicode_String(U"fonts"), iter.current));
	}

	if (fonts.count == 0)
	{
		abort_the_mission(U"Expected at least one font in 'fonts' folder");
		return;
	}

	ui_font = find_font(ui_font_name);
	if (!ui_font)
	{
		abort_the_mission(U"Font named '%' must be in 'fonts' folder. It is used to draw ui.", ui_font_name);
		return;
	}
}



Font* Typer::find_font(Unicode_String name)
{
	for (auto& font : fonts)
	{
		if (font.name == name)
		{
			return &font;
		}
	}
	return NULL;
}

Font* Typer::load_font(Unicode_String path)
{
	Font font;
	if (!::load_font(path, c_allocator, &font))
	{
		log(ctx.logger, U"Failed to load font at path: %", path);
		return NULL;
	}

	return fonts.add(font);
}





bool Input::is_key_down(Key key)
{
	for (Input_Key_State& pressed_key: pressed_keys)
	{
		if (pressed_key.key_code == key)
		{
			return pressed_key.action == Key_Action::Down;
			break;
		}
	}

	return false;
}

bool Input::is_key_down_or_held(Key key)
{
	for (Input_Key_State& pressed_key: pressed_keys)
	{
		if (pressed_key.key_code == key)
		{
			return (pressed_key.action == Key_Action::Down) || (pressed_key.action == Key_Action::Hold);
			break;
		}
	}

	return false;
}

bool Input::is_key_held(Key key)
{
	for (Input_Key_State& pressed_key: pressed_keys)
	{
		if (pressed_key.key_code == key)
		{
			return pressed_key.action == Key_Action::Hold;
			break;
		}
	}

	return false;
}

bool Input::is_key_up(Key key)
{
	for (Input_Key_State& pressed_key: pressed_keys)
	{
		if (pressed_key.key_code == key)
		{
			return pressed_key.action == Key_Action::Up;
			break;
		}
	}

	return false;
}

bool Input::is_key_combo_pressed(Dynamic_Array<Key> keys)
{	
	auto find_key_state = [&](Key key) -> Input_Key_State*
	{
		for (Input_Key_State& pressed_key: pressed_keys)
		{
			if (pressed_key.key_code == key) 
				return &pressed_key;
		}

		return NULL;
	};


	bool any_key_down = false;

	for (Key requested_key: keys)
	{
		Input_Key_State* key_state = find_key_state(requested_key);

		if (!key_state) return false;
	
		if (key_state->action == Key_Action::Down)
		{
			any_key_down = true;
			continue;
		}

		if (key_state->action != Key_Action::Hold)
		{
			return false;
		}
	}

	return any_key_down;
}
bool Input::is_key_combo_pressed(Key key_1, Key key_2)
{
	Key keys[2];
	keys[0] = key_1;
	keys[1] = key_2;

	return is_key_combo_pressed(Dynamic_Array<Key>::from_static_array(keys));
}



void Input::init()
{
	ZoneScoped;

	nodes = Dynamic_Array<Input_Node>(32, c_allocator);
	pressed_keys = Dynamic_Array<Input_Key_State>(32, c_allocator);
}

void Input::update_key_states()
{
	for (Input_Key_State& pressed_key: pressed_keys)
	{
		pressed_key.press_time += frame_time;

		if (pressed_key.action == Key_Action::Down)
		{
			pressed_key.action = Key_Action::Hold;
		}

		if (pressed_key.repeated)
		{
			pressed_key.repeated = false;
		}
	}

	for (int i = 0; i < pressed_keys.count; i++)
	{
		if (pressed_keys[i]->action == Key_Action::Up)
		{
			pressed_keys.remove_at_index(i);
			i--;
		}
	}

	if (!windows.has_window_focus)
	{
		for (Input_Key_State& pressed_key: pressed_keys)
		{
			pressed_key.action = Key_Action::Up;
		}

		return;
	}


	for (Input_Node& node : input.nodes)
	{
		if (node.input_type != Input_Type::Key) continue;

		if (node.key_action == Key_Action::Down)
		{
			bool key_exists = false;

			for (Input_Key_State& pressed_key: pressed_keys)
			{
				if (pressed_key.key_code == node.key)
				{
					key_exists = true;
					pressed_key.repeated = true;
					break;
				}
			}

			if (key_exists) continue;


			pressed_keys.add({
				.key_code = node.key,

				.press_time = 0.0,
				.action = Key_Action::Down,
			});
		}
		else if (node.key_action == Key_Action::Up)
		{
			for (Input_Key_State& pressed_key: pressed_keys)
			{
				if (pressed_key.key_code == node.key)
				{
					pressed_key.action = Key_Action::Up;
					break;
				}
			}
		}
	}


	if (input.is_key_down(Key::LMB))
	{
		SetCapture(windows.hwnd);
	}
	else if (input.is_key_up(Key::LMB))
	{
		SetCapture(NULL);
	}
}




void dot_position_for_statement_block_entry(Node* node, int* out_dot_x, int* out_dot_y)
{
	assert(node->node_type == Node_Type::Statement_Block);

	*out_dot_x = node->rect.center_x();
	*out_dot_y = node->rect.y_top;
}


Rect get_dot_rect(int x, int y)
{
	return Rect(x - dot_size / 2, y - dot_size / 2, x + dot_size / 2, y + dot_size / 2);
}



void Typer::draw_node(Node* node, int node_index_top_level, int depth)
{
	UI_ID ui_id = ui_id_uuid(node->uuid, 0);

	switch (node->node_type)
	{
		case Node_Type::Function:
		{
			if (node->rect.is_point_inside(input.mouse_x, input.mouse_y))
			{
				ui.im_hovering(ui_id);
			}

			if (ui.holding == ui_id)
			{
				node->rect.move(input.mouse_x_delta, input.mouse_y_delta);
				node->function_statement_block->rect.move(input.mouse_x_delta, input.mouse_y_delta);
			}

			bool hovering = ui.hover == ui_id;


			renderer.draw_rect(node->rect, hovering ? rgba(100, 100, 0, 255) : node->color);



			if (hovering && input.is_key_down(Key::LMB))
			{

			}

			{
				scoped_set_and_revert(ui.parameters.text_alignment, Text_Alignment::Center);
				scoped_set_and_revert(ui.parameters.center_text_vertically, true);

				ui.draw_text(node->rect.center_x(), node->rect.y_top - ui.get_font_face()->size, node->name);
			}


			int dot_x = node->rect.center_x();
			int dot_y = node->rect.y_bottom;


			if (node->function_statement_block)
			{
				int statement_block_entry_dot_x;
				int statement_block_entry_dot_y;

				dot_position_for_statement_block_entry(node->function_statement_block, &statement_block_entry_dot_x, &statement_block_entry_dot_y);

				renderer.draw_line(dot_x, dot_y, statement_block_entry_dot_x, statement_block_entry_dot_y, rgba(255, 255, 255, 255));

				draw_node(node->function_statement_block, node_index_top_level, depth + 1);
			}

			renderer.draw_rect(get_dot_rect(dot_x, dot_y), rgba(0, 200, 0, 255));
		}
		break;

		case Node_Type::Statement_Block:
		{
			if (node->rect.is_point_inside(input.mouse_x, input.mouse_y))
			{
				ui.im_hovering(ui_id);
			}

			if (ui.holding == ui_id)
			{
				node->rect.move(input.mouse_x_delta, input.mouse_y_delta);
			}

			bool hovering = ui.hover == ui_id;


			renderer.draw_rect(node->rect, hovering ? rgba(100, 100, 0, 255) : node->color);


			if (hovering && input.is_key_down(Key::LMB))
			{

			}

			{
				scoped_set_and_revert(ui.parameters.text_alignment, Text_Alignment::Center);
				scoped_set_and_revert(ui.parameters.center_text_vertically, true);

				ui.draw_text(node->rect.center_x(), node->rect.y_top - ui.get_font_face()->size, node->name);
			}


			int entry_dot_x = node->rect.center_x();
			int entry_dot_y = node->rect.y_top;

			renderer.draw_rect(get_dot_rect(entry_dot_x, entry_dot_y), rgba(0, 200, 0, 255));



			int statement_dot_x = node->rect.x_left + dot_size / 2 + 14;
			int statement_dot_y = node->rect.y_top - 100;
			for (int i = 0; i < node->sub_statements.count; i++)
			{
				Node* sub_statement = *node->sub_statements[i];

				defer{ statement_dot_y -= 100; };

				UI_ID statement_dot_ui_id = ui_id_uuid(node->uuid, i);

				Rect sub_statement_node_dot = sub_statement->get_node_dot();


				renderer.draw_line(statement_dot_x, statement_dot_y, sub_statement_node_dot.center_x(), sub_statement_node_dot.center_y(), rgba(255, 255, 255, 255));


				{
					scoped_set_and_revert(ui.parameters.text_alignment, Text_Alignment::Center);
					scoped_set_and_revert(ui.parameters.center_text_vertically, true);
					scoped_set_and_revert(ui.parameters.text_font_face_size, 9);


					renderer.draw_rect(get_dot_rect(statement_dot_x, statement_dot_y), rgba(0, 200, 0, 255));

					ui.draw_text(statement_dot_x, statement_dot_y, to_string(i, frame_allocator).to_unicode_string(frame_allocator));

					draw_node(sub_statement, node_index_top_level, depth + 1);
				}
			}
			
			{
				scoped_set_and_revert(ui.parameters.text_alignment, Text_Alignment::Center);
				scoped_set_and_revert(ui.parameters.center_text_vertically, true);
				scoped_set_and_revert(ui.parameters.text_font_face_size, 9);


				Rect dot_rect = get_dot_rect(statement_dot_x, statement_dot_y);
				UI_ID statement_dot_ui_id = ui_id_uuid(node->uuid, 0);


				if (dot_rect.is_point_inside(input.mouse_x, input.mouse_y))
				{
					ui.im_hovering(statement_dot_ui_id);
				}

				rgba color = rgba(0, 200, 0, 255);
				if (ui.hover == statement_dot_ui_id)
				{
					color = rgba(100, 200, 0, 255);
				}
				
				if (ui.down == statement_dot_ui_id)
				{
					Node* new_statement = add_node();

					new_statement->name = U"Statement";

					new_statement->rect = Rect(statement_dot_x + 100, statement_dot_y - 100, statement_dot_x + 100 + 500, statement_dot_y + 100);

					new_statement->color = rgba(100, 0, 100, 255);

					new_statement->node_type = Node_Type::Statement;

					new_statement->uuid = get_uuid();

					new_statement->statement_type = Statement_Type::None;

					node->sub_statements.add(new_statement);
				}

				renderer.draw_rect(dot_rect, color);

				ui.draw_text(statement_dot_x, statement_dot_y, U"+");
			}
		}
		break;

		case Node_Type::Statement:
		{
			if (node->rect.is_point_inside(input.mouse_x, input.mouse_y))
			{
				ui.im_hovering(ui_id);
			}

			if (ui.holding == ui_id)
			{
				node->rect.move(input.mouse_x_delta, input.mouse_y_delta);
			}

			bool hovering = ui.hover == ui_id;


			renderer.draw_rect(node->rect, hovering ? rgba(100, 100, 0, 255) : node->color);


			{
				scoped_set_and_revert(ui.parameters.text_alignment, Text_Alignment::Center);
				scoped_set_and_revert(ui.parameters.center_text_vertically, true);

				ui.draw_text(node->rect.center_x(), node->rect.y_top - ui.get_font_face()->size, node->name);
			}


			// Statement type
			{
				using namespace Reflection;

				auto options = make_array<Unicode_String>(32, frame_allocator);

				int selected_index = -1;

				Enum_Type* statement_type_type = (Enum_Type*) Reflection::type_of<Statement_Type>();
				for (auto& value: statement_type_type->values)
				{
					options.add(value.name.to_unicode_string(frame_allocator));
					if (memcmp(&value.value, &node->statement_type, sizeof(node->statement_type)) == 0)
					{
						assert(selected_index == -1);
						selected_index = statement_type_type->values.fast_pointer_index(&value);
					}
				}

				int new_selected_index;
				if (ui.dropdown(Rect(node->rect.x_left + renderer.scaled(20), node->rect.y_top - renderer.scaled(48), node->rect.x_left + renderer.scaled(20) + renderer.scaled(64), node->rect.y_top - renderer.scaled(24)), selected_index, options, &new_selected_index, ui_id_uuid(node->uuid, 0)))
				{
					memcpy(&node->statement_type, &statement_type_type->values[new_selected_index]->value, sizeof(node->statement_type));


					switch (node->statement_type)
					{
						case Statement_Type::If:
						{
							node->if_predicate = NULL;
							node->if_body = NULL;
						}
						break;

						case Statement_Type::Jump:
						{
							node->jump_target = NULL;
						}
						break;

						case Statement_Type::Variable:
						{
							node->variable_type = NULL;
							node->variable_name = Unicode_String(U"Dummy name").copy_with(c_allocator);
						}
						break;
					}
				}
			}

			switch (node->statement_type)
			{
				case Statement_Type::If:
				{
					Rect if_predicate_dot = node->get_if_predicate_dot();
					Rect if_body_dot = node->get_if_body_dot();

					if (dragging_node)
					{
						if (ui.up == dragging_node_ui_id && if_predicate_dot.is_point_inside(input.mouse_x, input.mouse_y))
						{
							node->if_predicate = dragging_node;
						}

						if (ui.up == dragging_node_ui_id && if_body_dot.is_point_inside(input.mouse_x, input.mouse_y))
						{
							node->if_body = dragging_node;
						}
					}


					if (node->if_predicate)
					{
						Node* predicate = node->if_predicate;

						Rect if_predicate_node_dot = predicate->get_node_dot();

						renderer.draw_line(if_predicate_dot.center_x(), if_predicate_dot.center_y(), if_predicate_node_dot.center_x(), if_predicate_node_dot.center_y(), rgba(255, 255, 255, 255));
					}

					if (node->if_body)
					{
						Node* body = node->if_body;

						Rect if_body_node_dot = body->get_node_dot();

						renderer.draw_line(if_body_dot.center_x(), if_body_dot.center_y(), if_body_node_dot.center_x(), if_body_node_dot.center_y(), rgba(255, 255, 255, 255));
					}

					renderer.draw_rect(if_predicate_dot, rgba(0, 255, 0, 255));
					renderer.draw_rect(if_body_dot, rgba(0, 255, 0, 255));

					{
						scoped_set_and_revert(ui.parameters.text_alignment, Text_Alignment::Center);
						scoped_set_and_revert(ui.parameters.center_text_vertically, true);
						scoped_set_and_revert(ui.parameters.text_font_face_size, 8);

						ui.draw_text(if_predicate_dot.center_x(), if_predicate_dot.y_top + renderer.scaled(8), U"Predicate");
						ui.draw_text(if_body_dot.center_x(), if_body_dot.y_top + renderer.scaled(8), U"Body");
					}
				}
				break;

				case Statement_Type::Jump:
				{
					Rect jump_target_dot = node->get_jump_target_dot();

					if (dragging_node)
					{
						if (ui.up == dragging_node_ui_id && jump_target_dot.is_point_inside(input.mouse_x, input.mouse_y))
						{
							node->jump_target = dragging_node;
						}
					}


					if (node->jump_target)
					{
						Node* jump_target = node->jump_target;

						Rect jump_target_node_dot = jump_target->get_node_dot();

						renderer.draw_line(jump_target_dot.center_x(), jump_target_dot.center_y(), jump_target_node_dot.center_x(), jump_target_node_dot.center_y(), rgba(255, 255, 255, 255));
					}

					renderer.draw_rect(jump_target_dot, rgba(0, 255, 0, 255));
					{
						scoped_set_and_revert(ui.parameters.text_alignment, Text_Alignment::Center);
						scoped_set_and_revert(ui.parameters.center_text_vertically, true);
						scoped_set_and_revert(ui.parameters.text_font_face_size, 8);

						ui.draw_text(jump_target_dot.center_x(), jump_target_dot.y_top + renderer.scaled(8), U"Jump target");
					}
				}
				break;

				case Statement_Type::Variable:
				{
					Rect variable_type_dot = node->get_variable_type_dot();

					if (dragging_node)
					{
						if (ui.up == dragging_node_ui_id && variable_type_dot.is_point_inside(input.mouse_x, input.mouse_y))
						{
							node->variable_type = dragging_node;
						}
					}


					if (node->variable_type)
					{
						Node* variable_type = node->variable_type;

						Rect variable_type_node_dot = variable_type->get_node_dot();

						renderer.draw_line(variable_type_dot.center_x(), variable_type_dot.center_y(), variable_type_node_dot.center_x(), variable_type_node_dot.center_y(), rgba(255, 255, 255, 255));
					}

					renderer.draw_rect(variable_type_dot, rgba(0, 255, 0, 255));

					{
						scoped_set_and_revert(ui.parameters.text_alignment, Text_Alignment::Center);
						scoped_set_and_revert(ui.parameters.center_text_vertically, true);
						scoped_set_and_revert(ui.parameters.text_font_face_size, 8);

						ui.draw_text(variable_type_dot.center_x(), variable_type_dot.y_top + renderer.scaled(8), U"Variable type");
					}


					{
						Unicode_String new_variable_name;
						if (ui.text_editor(Rect(node->rect.x_left + renderer.scaled(100), node->rect.y_top - renderer.scaled(96), node->rect.x_left + renderer.scaled(100) + renderer.scaled(192), node->rect.y_top - renderer.scaled(96) + renderer.scaled(24)), node->variable_name, &new_variable_name, ui_id_uuid(node->uuid, 0)))
						{
							c_allocator.free(node->variable_name.data, code_location());

							node->variable_name = new_variable_name.copy_with(c_allocator);
						}
					}

				}
				break;
			}
		}
		break;

		case Node_Type::Type_Declaration:
		{
			if (node->rect.is_point_inside(input.mouse_x, input.mouse_y))
			{
				ui.im_hovering(ui_id);
			}

			bool hovering = ui.hover == ui_id;

			if (ui.holding == ui_id)
			{
				node->rect.move(input.mouse_x_delta, input.mouse_y_delta);
			}

			renderer.draw_rect(node->rect, hovering ? rgba(100, 100, 0, 255) : node->color);


			{
				scoped_set_and_revert(ui.parameters.text_alignment, Text_Alignment::Center);
				scoped_set_and_revert(ui.parameters.center_text_vertically, true);
				scoped_set_and_revert(ui.parameters.text_font_face_size, 10);


				ui.draw_text(node->rect.center_x(), node->rect.y_top - ui.get_font_face()->size, node->name);
			}

			if (node->type_kind == Type_Kind::Struct)
			{
				int y = node->rect.y_top - renderer.scaled(54);
				for (Struct_Member& member: node->struct_members)
				{
					Rect member_type_dot = Rect(node->rect.x_left + renderer.scaled(24), y - renderer.scaled(24), node->rect.x_left + renderer.scaled(24) + renderer.scaled(24), y);


					renderer.draw_rect(member_type_dot, rgba(0, 255, 0, 255));

					if (dragging_node)
					{
						if (ui.up == dragging_node_ui_id && member_type_dot.is_point_inside(input.mouse_x, input.mouse_y))
						{
							member.type = dragging_node;
						}
					}

					if (member.type)
					{
						Rect member_type_node_dot = member.type->get_node_dot();

						renderer.draw_line(member_type_dot.center_x(), member_type_dot.center_y(), member_type_node_dot.center_x(), member_type_node_dot.center_y(), rgba(255, 255, 255, 255));
					}

					scoped_set_and_revert(ui.parameters.text_alignment, Text_Alignment::Left);
					scoped_set_and_revert(ui.parameters.center_text_vertically, true);
					scoped_set_and_revert(ui.parameters.text_font_face_size, 10);
					ui.draw_text(node->rect.x_left + renderer.scaled(56), member_type_dot.center_y(), member.name);

					y -= renderer.scaled(48);
				}

				if (ui.button(Rect(node->rect.x_left, y - renderer.scaled(24), node->rect.x_right, y), U"Add a member", rgba(50, 25, 25, 255), ui_id_uuid(node->uuid, 0)))
				{
					Struct_Member new_member = {
						.name = U"New member",
						.type = NULL,
					};

					node->struct_members.add(new_member);
				}
			}
		}
		break;

		case Node_Type::Variable_Value:
		{
			if (node->rect.is_point_inside(input.mouse_x, input.mouse_y))
			{
				ui.im_hovering(ui_id);
			}

			bool hovering = ui.hover == ui_id;

			if (ui.holding == ui_id)
			{
				node->rect.move(input.mouse_x_delta, input.mouse_y_delta);
			}

			renderer.draw_rect(node->rect, hovering ? rgba(100, 100, 0, 255) : node->color);


			{
				scoped_set_and_revert(ui.parameters.text_alignment, Text_Alignment::Center);
				scoped_set_and_revert(ui.parameters.center_text_vertically, true);
				scoped_set_and_revert(ui.parameters.text_font_face_size, 10);


				ui.draw_text(node->rect.center_x(), node->rect.y_top - ui.get_font_face()->size, node->name);
			}


			Rect variable_type_dot = node->get_variable_type_type_dot();

			if (dragging_node)
			{
				if (ui.up == dragging_node_ui_id && variable_type_dot.is_point_inside(input.mouse_x, input.mouse_y))
				{
					if (dragging_node->node_type == Node_Type::Type_Declaration)
					{
						node->variable_value_type = dragging_node;

						if (dragging_node->type_kind == Type_Kind::Primitive)
						{
							memset(&node->primitive_value, 0, sizeof(node->primitive_value));
						}
						else if (dragging_node->type_kind == Type_Kind::Struct)
						{
							node->struct_member_values = make_array<Node*>(32, c_allocator); // @MemoryLeak 

							for (Struct_Member& member: dragging_node->struct_members)
							{
								node->struct_member_values.add(NULL);
							}
						}
					}
					else
					{
						log(ctx.logger, U"Type declaration is required!!");
					}
				}
			}

			renderer.draw_rect(variable_type_dot, rgba(0, 255, 0, 255));

			{
				scoped_set_and_revert(ui.parameters.text_alignment, Text_Alignment::Center);
				scoped_set_and_revert(ui.parameters.center_text_vertically, true);
				scoped_set_and_revert(ui.parameters.text_font_face_size, 10);

				ui.draw_text(variable_type_dot.center_x(), variable_type_dot.y_top + ui.get_font_face()->baseline_offset, U"Variable value");
			}

			if (node->variable_value_type)
			{
				if (node->variable_value_type->type_kind == Type_Kind::Primitive)
				{
					Reflection::Primitive_Type* primitive_type = Reflection::get_primitive_type(map_primitive_kind_to_reflection_primitive_kind(node->variable_value_type->primitive_kind));

					Unicode_String new_primitive_value_string;

					if (ui.text_editor(Rect(node->rect.x_left + renderer.scaled(14), node->rect.y_top - renderer.scaled(128), node->rect.x_right - renderer.scaled(14), node->rect.y_top - renderer.scaled(128) + renderer.scaled(32)), primitive_to_string(primitive_type, &node->primitive_value, frame_allocator).to_unicode_string(frame_allocator), &new_primitive_value_string, ui_id_uuid(node->uuid, 0)))
					{
						if (!parse_primitive(new_primitive_value_string.to_utf8_but_ascii(frame_allocator), primitive_type, &node->primitive_value))
						{
							log(ctx.logger, U"Failed to parse primitive");
						}
					}
				}
				else if (node->variable_value_type->type_kind == Type_Kind::Struct)
				{
					int y = node->rect.y_top - renderer.scaled(72);

					for (int i = 0; i < node->struct_member_values.count; i++)
					{
						Rect struct_member_value_dot = Rect(node->rect.x_left + renderer.scaled(24), y - renderer.scaled(24), node->rect.x_left + renderer.scaled(24) + renderer.scaled(24), y);

						renderer.draw_rect(struct_member_value_dot, rgba(0, 255, 0, 255));

						Struct_Member* member = node->variable_value_type->struct_members[i];

						{
							scoped_set_and_revert(ui.parameters.text_alignment, Text_Alignment::Left);
							scoped_set_and_revert(ui.parameters.center_text_vertically, true);
							scoped_set_and_revert(ui.parameters.text_font_face_size, 10);

							ui.draw_text(struct_member_value_dot.x_right + renderer.scaled(8), struct_member_value_dot.center_y(), member->name);
						}

						if (dragging_node)
						{
							if (ui.up == dragging_node_ui_id && struct_member_value_dot.is_point_inside(input.mouse_x, input.mouse_y))
							{
								node->struct_member_values.data[i] = dragging_node;
							}
						}


						Node* struct_member_value_node = node->struct_member_values.data[i];

						if (struct_member_value_node)
						{
							Rect target_node_dot = struct_member_value_node->get_node_dot();

							renderer.draw_line(struct_member_value_dot.center_x(), struct_member_value_dot.center_y(), target_node_dot.center_x(), target_node_dot.center_y(), rgba(255, 255, 255, 255));
						}

						y -= renderer.scaled(36);
					}
				}
				else
				{
					assert(false);
				}
			}
		}
		break;
	}


	{
		Rect node_dot = node->get_node_dot();
		UI_ID node_dot_ui_id = ui_id_uuid(node->uuid, 0);

		struct Node_Dot_State
		{
			int drag_start_x;
			int drag_start_y;
		};

		Node_Dot_State* state = (decltype(state)) ui.get_ui_item_data(ui_id);

		if (!state)
		{
			state = (decltype(state)) c_allocator.alloc(sizeof(*state), code_location());
			ui.ui_id_data_array.put(ui_id, state);

			state->drag_start_x = 0;
			state->drag_start_y = 0;
		}

		if (node_dot.is_point_inside(input.mouse_x, input.mouse_y))
		{
			ui.im_hovering(node_dot_ui_id);	
		}

		renderer.draw_rect(node_dot, rgba(0, 255, 0, 255));

		if (ui.down == node_dot_ui_id)
		{
			state->drag_start_x = input.mouse_x;
			state->drag_start_y = input.mouse_y;
		}

		if (ui.holding == node_dot_ui_id)
		{
			dragging_node_this_frame = node;
			dragging_node_ui_id_this_frame = node_dot_ui_id;


			renderer.draw_rect(node_dot.moved(input.mouse_x - state->drag_start_x, input.mouse_y - state->drag_start_y), rgba(255, 10, 10, 255));
		}

		if (ui.up == node_dot_ui_id)
		{
			log(ctx.logger, U"Ditch");
		}
	}
}

Node* Typer::add_node()
{
	Node* node = nodes.add();
	*node = Node();
	node->uuid = get_uuid();
	return node;
}


void Typer::do_frame()
{
	defer { FrameMark; };

	defer{ frame_index += 1; };

	defer {
		if (frame_index == 0)
		{
			log(ctx.logger, U"Time from start to first frame done: % ms", time_to_from_start_to_first_frame_done.ms_elapsed_double());
		}
	};

	frame_allocator.reset();

	frame_time = time_measurer.seconds_elapsed_double();
	time_measurer.reset();


	{
		input.mouse_x_delta = input.mouse_x - input.old_mouse_x;
		input.mouse_y_delta = input.mouse_y - input.old_mouse_y;

		input.old_mouse_x = input.mouse_x;
		input.old_mouse_y = input.mouse_y;

		input.update_key_states();
	}
	defer { input.mouse_wheel_delta = 0; };
	defer{ input.nodes.clear(); };
	
	// @Cleanup:
	//next_redraw_frame = 0;
	next_redraw_frame = frame_index;


	redraw_current_frame = (next_redraw_frame == frame_index);

	
	if (windows.window_height == 0 && windows.window_width == 0)
	{
		return;
	}




	{
		ui_font_face = ui_font->get_face(renderer.scaled(ui_font_face_size));
	}





	if (windows.window_size_changed)
	{
		renderer.resize(windows.window_width, windows.window_height);
		windows.window_size_changed = false;
		redraw_current_frame = true;
	}

	ui.pre_frame();
	defer { ui.post_frame(); };


	if ((ui.holding != invalid_ui_id) || (input.mouse_x_delta || input.mouse_y_delta))
	{
		redraw_current_frame = true;
	}

	if (input.mouse_wheel_delta)
	{
		redraw_current_frame = true;
	}


	if (input.nodes.count)
	{
		redraw_current_frame = true;
	}

	if (input.pressed_keys.count)
	{
		redraw_current_frame = true;
	}



	if (input.is_key_down(Key::F6))
	{
		renderer.use_dpi_scaling = !renderer.use_dpi_scaling;
	}

	windows.window_dpi = GetDpiForWindow(windows.hwnd);
	windows.window_scaling = float(windows.window_dpi) / 96.0;

	if (renderer.use_dpi_scaling)
	{
		renderer.scaling = windows.window_scaling;
	}
	else
	{
		renderer.scaling = 1.0;
	}

	


	if (redraw_current_frame)
	{
		renderer.clear();

		renderer.mask_stack.count = 0;
		renderer.recalculate_mask_buffer();
	}



	dragging_node_this_frame = NULL;
	dragging_node_ui_id_this_frame = null_ui_id;
	defer
	{
		dragging_node = dragging_node_this_frame;
	 	dragging_node_ui_id = dragging_node_ui_id_this_frame;
	};




	if (frame_index == 0)
	{
		{
			auto add_primitive_type_node = [&](int x, int y, Primitive_Kind kind)
			{
				Node* node = add_node();

				node->node_flags |= NODE_TOP_DECL;
				
				node->name = format_unicode_string(c_allocator, U"Primitive: %", kind);

				node->rect = Rect(x, y, x + renderer.scaled(192), y + renderer.scaled(32));
				node->color = rgba(100, 0, 100, 255);

				node->node_type = Node_Type::Type_Declaration;

				node->type_kind = Type_Kind::Primitive;

				node->primitive_kind = kind;
			};



			int x = 54;
			int y = 54;

			const int y_advance = 64;

			add_primitive_type_node(x, y, Primitive_Kind::P_u8);
			y += y_advance;
			add_primitive_type_node(x, y, Primitive_Kind::P_u16);
			y += y_advance;
			add_primitive_type_node(x, y, Primitive_Kind::P_u32);
			y += y_advance;
			add_primitive_type_node(x, y, Primitive_Kind::P_u64);
			y += y_advance;

			add_primitive_type_node(x, y, Primitive_Kind::P_s8);
			y += y_advance;
			add_primitive_type_node(x, y, Primitive_Kind::P_s16);
			y += y_advance;
			add_primitive_type_node(x, y, Primitive_Kind::P_s32);
			y += y_advance;
			add_primitive_type_node(x, y, Primitive_Kind::P_s64);
			y += y_advance;

			y += 32;

			add_primitive_type_node(x, y, Primitive_Kind::P_f32);
			y += y_advance;
			add_primitive_type_node(x, y, Primitive_Kind::P_f64);
			y += y_advance;

			y += 32;

			add_primitive_type_node(x, y, Primitive_Kind::P_bool);
			y += y_advance;
		}



		// Init

		Node* new_node = add_node();


		new_node->node_flags |= NODE_TOP_DECL;

		new_node->name = Unicode_String(U"main").copy_with(c_allocator);


		int x = renderer.width / 2;
		int y = renderer.height / 2;

		new_node->rect = Rect(x - 256, y - 50, x + 256, y + 50);
		new_node->color = rgba(100, 20, 20, 255);


		new_node->node_type = Node_Type::Function;

		new_node->function_arguments = make_array<Function_Argument>(32, c_allocator);


		{
			Node* statement_block_node = add_node();

			new_node->function_statement_block = statement_block_node;

			statement_block_node->name = U"Statement block";

			statement_block_node->rect = Rect(new_node->rect.x_left, new_node->rect.y_bottom - 512, new_node->rect.x_right, new_node->rect.y_bottom);

			statement_block_node->rect.move(0, -72);

			statement_block_node->color = rgba(0, 100, 0, 255);
			statement_block_node->node_type = Node_Type::Statement_Block;

			statement_block_node->sub_statements = make_array<Node*>(32, c_allocator);

			{
				Node* first_statement = nodes.add();
				*first_statement = Node();


				first_statement->name = U"Statement";
				first_statement->rect = Rect(statement_block_node->rect.x_left, statement_block_node->rect.y_top - 48, statement_block_node->rect.x_right, statement_block_node->rect.y_top);

				first_statement->color = rgba(100, 0, 100, 255);
				first_statement->node_type = Node_Type::Statement;
			}

		}

	}



	{
		scoped_set_and_revert(ui.parameters.text_font_face_size, 12);

		if (ui.button(Rect(0, renderer.height - renderer.scaled(48), renderer.scaled(192), renderer.height), U"Spawn struct declaration", rgba(100, 25, 25, 255), ui_id(0)))
		{
			Node* new_node = add_node();

			new_node->node_flags |= NODE_TOP_DECL;

			new_node->name = Unicode_String(U"Struct. MAKE THAT I CAN SET NAME").copy_with(c_allocator);

			new_node->rect = Rect(renderer.scaled(192), renderer.height - renderer.scaled(192), renderer.scaled(192) + renderer.scaled(192), renderer.height);
			new_node->color = rgba(0, 20, 100, 255);

			new_node->node_type = Node_Type::Type_Declaration;


			new_node->type_kind = Type_Kind::Struct;

			new_node->struct_members = make_array<Struct_Member>(32, c_allocator);
		}

		if (ui.button(Rect(0, renderer.height - renderer.scaled(56) - renderer.scaled(48), renderer.scaled(192), renderer.height - renderer.scaled(56)), U"Spawn value", rgba(100, 25, 25, 255), ui_id(0)))
		{
			Node* new_node = add_node();

			new_node->node_flags |= NODE_TOP_DECL;

			new_node->name = Unicode_String(U"Value").copy_with(c_allocator);

			new_node->rect = Rect(renderer.scaled(192), renderer.height - renderer.scaled(192), renderer.scaled(192) + renderer.scaled(192), renderer.height);
			new_node->color = rgba(0, 20, 100, 255);

			new_node->node_type = Node_Type::Variable_Value;

			new_node->variable_value_type = NULL;
		}
	}





	{
		bool was_lmb_used = false;

		for (int i = 0; i < nodes.count; i++)
		{
			Node* node = nodes[i];
			if (!(node->node_flags & NODE_TOP_DECL)) continue;

			draw_node(node, i, 0);
		}




		if (false && input.is_key_down(Key::LMB) && !was_lmb_used)
		{
			Node* new_node =  add_node();

			new_node->node_flags |= NODE_TOP_DECL;


			new_node->name = Unicode_String(U"Function").copy_with(c_allocator);


			new_node->rect = Rect(input.mouse_x - 256, input.mouse_y - 50, input.mouse_x + 256, input.mouse_y + 50);
			new_node->color = rgba(100, 20, 20, 255);


			new_node->node_type = Node_Type::Function;

			new_node->function_arguments = make_array<Function_Argument>(32, c_allocator);


			{
				Node* statement_block_node = add_node();
				new_node->function_statement_block = statement_block_node;

				statement_block_node->name = U"Statement block";

				statement_block_node->rect = Rect(new_node->rect.x_left, new_node->rect.y_bottom - 512, new_node->rect.x_right, new_node->rect.y_bottom);

				statement_block_node->color = rgba(0, 100, 0, 255);
				statement_block_node->node_type = Node_Type::Statement_Block;

				statement_block_node->sub_statements = make_array<Node*>(32, c_allocator);

			}

		}

	}





	{
		if (do_show_fps)
		{
			Unicode_String fps_str = format_unicode_string(frame_allocator, U"AVG FPS: %, FPS: %, ms: %",
				Unicode_String::from_ascii(to_string(avg_fps_last, frame_allocator), frame_allocator),
				Unicode_String::from_ascii(to_string(1 / frame_time, frame_allocator, 0), frame_allocator),
				Unicode_String::from_ascii(to_string(frame_time * 1000.0, frame_allocator, 4), frame_allocator));

			int fps_str_width = measure_text_width(fps_str, ui_font_face);
			renderer.draw_text(ui_font_face, fps_str, renderer.width - fps_str_width, ui_font_face->baseline_offset);
		}

		frame_time_accum += frame_time;
		frame_time_accum_count += 1;

		if (frame_time_accum_count > avg_fps_refresh_rate)
		{
			avg_fps_last =  1.0f / (frame_time_accum / float(frame_time_accum_count));

			frame_time_accum = 0;
			frame_time_accum_count = 0;
		}
	}



	if (redraw_current_frame)
	{
		TracyCZoneNC(framebuffer_copy_zone, "Copy renderer.framebuffer to windows_friendly_framebuffer", 0xff0000, true);

		u32 windows_framebuffer_width = align(renderer.width, 4);
		u64 windows_framebuffer_size = 3 * windows_framebuffer_width * renderer.height;

		TracyCZoneNC(allocate_windows_friendly_zone, "allocate windows friendly framebuffer", 0xff0000, true);
		u8* windows_friendly_framebuffer = allocate<u8>(windows_framebuffer_size, frame_allocator);
		TracyCZoneEnd(allocate_windows_friendly_zone);

		
		{
			for (int y = 0; y < renderer.height; y++)
			{
				//ZoneScopedN("copy line");

				int windows_framebuffer_y_offset  = y * windows_framebuffer_width * 3;
				int renderer_framebuffer_y_offset = y * renderer.width * renderer.channels_count;

				u8* dest = windows_friendly_framebuffer + windows_framebuffer_y_offset;
				u8* src  = renderer.framebuffer + renderer_framebuffer_y_offset;


				const int batch_size = 128 / 32;

				int times     = renderer.width / batch_size;
				int remainder = renderer.width % batch_size; // @TODO: maybe just align renderer's lines by 8, so we can get rid of remainder????


#if 1 && TYPER_SIMD
				for (int i = 0; i < times; i++)
				{

					__m128i source         = _mm_lddqu_si128((__m128i*) src);
					__m128i windows_pixels = _mm_shuffle_epi8(source, _mm_set_epi8(0,
						16, 17, 18,   12, 13, 14,   8, 9, 10,     4, 5, 6,      0, 1, 2)); // Skip alpha byte and shuffle rgb to bgr.

					_mm_storeu_si128((__m128i*) dest, windows_pixels);

					dest += 3 * batch_size;
					src  += renderer.channels_count * batch_size;
				}

				int remaining_start = times * batch_size;
				for (int x = remaining_start; x < (remaining_start + remainder); x++)
				{
					dest[0] = src[2];
					dest[1] = src[1];
					dest[2] = src[0];

					src  += renderer.channels_count;
					dest += 3;
				}
#else
				for (int x = 0; x < renderer.width; x++)
				{
					// Windows has b,g,r pixel color order.
					dest[0] = src[2];
					dest[1] = src[1];
					dest[2] = src[0];
					
					dest += 3;
					src += renderer.channels_count;
				}
#endif

			}
		}

		TracyCZoneEnd(framebuffer_copy_zone);

		TracyCZoneNC(gdi_set_zone, "SetDIBitsToDevice", 0xff0000, true);

		// Draw on the dc
		{
			BITMAPINFO bi = { };
			memset(&bi, 0, sizeof(bi));

			bi.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
			bi.bmiHeader.biWidth  = windows_framebuffer_width;
			bi.bmiHeader.biHeight = renderer.height;
			bi.bmiHeader.biPlanes = 1;
			bi.bmiHeader.biBitCount = 24;
			bi.bmiHeader.biCompression = BI_RGB;
			bi.bmiHeader.biSizeImage = 0;

			int result = SetDIBitsToDevice(windows.dc,
				0, 0, windows_framebuffer_width, renderer.height,
				0, 0, 0, renderer.height,
				(void*) windows_friendly_framebuffer, &bi, DIB_RGB_COLORS);

		}

		TracyCZoneEnd(gdi_set_zone);
	}

	//threading.sleep(16); // @TODO: Do smth smarter
}


void set_working_directory_as_executable_directory()
{
	WCHAR dest[MAX_PATH];

	int length = GetModuleFileNameW(NULL, dest, MAX_PATH);
	PathCchRemoveFileSpec(dest, length);

	if (!SetCurrentDirectoryW(dest))
	{
		assert(false);
	}
}

int main()
{
	SetThreadDpiAwarenessContext(DPI_AWARENESS_CONTEXT_PER_MONITOR_AWARE_V2);


	set_working_directory_as_executable_directory();
	
	{
		ZoneScopedN("Open log file");

		SYSTEMTIME system_time;
		GetLocalTime(&system_time);


		Unicode_String str = format_unicode_string(c_allocator, U"logs/vis_last_log_%.%.%-%.%.%.txt", system_time.wDay, system_time.wMonth, system_time.wYear, system_time.wHour, system_time.wMinute, system_time.wSecond);
		defer { c_allocator.free(str.data, code_location()); };

		log_file = open_file(c_allocator, str, File_Open_Mode(FILE_WRITE | FILE_CREATE_NEW));
		if (!log_file.succeeded_to_open())
		{
			log(ctx.logger, U"Failed to open log file with name: %", str);
		}

		ctx.logger.concat_allocator = c_allocator;
		ctx.logger.log_proc = [](Unicode_String str)
		{
			int utf8_length;
			char* utf8_str = str.to_utf8(c_allocator, &utf8_length);
			fwrite(utf8_str, 1, utf8_length, stdout);
			fwrite("\n", 1, 1, stdout);

			if (log_file.succeeded_to_open())
			{
				log_file.write((u8*) utf8_str, utf8_length);
				log_file.write((u8*) "\n", 1);
				log_file.flush();
			}
			
			c_allocator.free(utf8_str, code_location());
		};
	}



	if (IsDebuggerPresent())
	{
		AllocConsole();
		AttachConsole(GetCurrentProcessId());

		int m_nCRTIn = _open_osfhandle(
			(long) GetStdHandle(STD_INPUT_HANDLE),
			_O_TEXT);

		FILE* m_fpCRTIn = _fdopen(m_nCRTIn, "r");

		FILE m_fOldStdIn = *stdin;
		*stdin = *m_fpCRTIn;

		int m_nCRTOut = _open_osfhandle(
			(long) GetStdHandle(STD_OUTPUT_HANDLE),
			_O_TEXT);

		FILE* m_fpCRTOut = _fdopen(m_nCRTOut, "w");
		
		setvbuf(stdout, NULL, _IONBF, 0);
		freopen_s(&m_fpCRTOut, "CONOUT$", "w", stdout);
	}



	// This should be taken from WinMain, but
	// GetModuleHandle(NULL) will give us the same value. 
	HINSTANCE hInstance = GetModuleHandle(NULL);

	const wchar_t CLASS_NAME[]  = L"Vis";

	WNDCLASS wc = { };

	wc.hInstance     = hInstance;
	wc.lpszClassName = CLASS_NAME;

	wc.lpfnWndProc = [](HWND h, UINT m, WPARAM wParam, LPARAM lParam) -> LRESULT
	{
		switch (m)
		{
			case WM_CLOSE:
			{
				PostQuitMessage(0);
			}
			break;

			case WM_SIZE:
			{
				windows.window_width  = LOWORD(lParam);
				windows.window_height = HIWORD(lParam);
				windows.window_size_changed = true;

				typer.do_frame();
			}
			break;			
			
			case WM_GETMINMAXINFO:
			{
				LPMINMAXINFO lpMMI = (LPMINMAXINFO) lParam;
				lpMMI->ptMinTrackSize.x = 256;
				lpMMI->ptMinTrackSize.y = 256;
			}
			break;

			case WM_CHAR:
			{
				if (wParam < 32) return true;

				switch (wParam)
				{
					case VK_BACK:
					case VK_TAB:
					case VK_RETURN:
					case VK_ESCAPE:
					case 127: // Ctrl + backspace
					{
						return true;
					}
				}

				u64 c = (u64) wParam;

				char32_t utf32_c;
				{
					if ((c & 0b1111'1100'0000'0000) == 0b1101'1000'0000'0000)
					{
						u16 lower = (u16) (c >> 16);
						u16 higher = (u16) (c);

						utf32_c = (higher - 0xD800) << 10;

						utf32_c |= ((char32_t) lower) - 0xDC00;

						utf32_c += 0x10000;
					}
					else
					{
						utf32_c = (char32_t) c;
					}
				}

				Input_Node node;
				node.input_type = Input_Type::Char;
				node.character = utf32_c;

				input.nodes.add(node);
			}
			break;

			case WM_KEYDOWN:
			case WM_SYSKEYDOWN:
			{
				Input_Node node;
				node.input_type = Input_Type::Key;
				node.key_action = Key_Action::Down;
				node.key = (Key) wParam;

				input.nodes.add(node);
			}
			break;

			case WM_KEYUP:
			case WM_SYSKEYUP:
			{
				Input_Node node;
				node.input_type = Input_Type::Key;
				node.key_action = Key_Action::Up;
				node.key = (Key) wParam;

				input.nodes.add(node);
			}
			break;

			case WM_MOUSEWHEEL:
			{
				input.mouse_wheel_delta = -GET_WHEEL_DELTA_WPARAM(wParam) / WHEEL_DELTA;
			}
			break;



			case WM_ACTIVATE:
			{
				windows.has_window_focus = wParam ? true : false;
			}
			break;

			case WM_MOUSEMOVE:
			{
				input.mouse_x = GET_X_LPARAM(lParam); 
				input.mouse_y = renderer.height - GET_Y_LPARAM(lParam);

				break;
			}

			case WM_LBUTTONDOWN:
			{
				Input_Node node;
				node.input_type = Input_Type::Key;
				node.key_action = Key_Action::Down;
				node.key = (Key) VK_LBUTTON;

				input.nodes.add(node);
			}
			break;

			case WM_RBUTTONDOWN:
			{
				Input_Node node;
				node.input_type = Input_Type::Key;
				node.key_action = Key_Action::Down;
				node.key = (Key) VK_RBUTTON;

				input.nodes.add(node);
			}
			break;
			case WM_MBUTTONDOWN:
			{
				Input_Node node;
				node.input_type = Input_Type::Key;
				node.key_action = Key_Action::Down;
				node.key = (Key) VK_MBUTTON;

				input.nodes.add(node);
			}
			break;

			case WM_NCLBUTTONUP:
			case WM_LBUTTONUP:
			{
				Input_Node node;
				node.input_type = Input_Type::Key;
				node.key_action = Key_Action::Up;
				node.key = (Key) VK_LBUTTON;

				input.nodes.add(node);
			}
			break;

			case WM_NCRBUTTONUP:
			case WM_RBUTTONUP:
			{
				Input_Node node;
				node.input_type = Input_Type::Key;
				node.key_action = Key_Action::Up;
				node.key = (Key) VK_RBUTTON;

				input.nodes.add(node);
			}
			break;
			
			case WM_NCMBUTTONUP:
			case WM_MBUTTONUP:
			{
				Input_Node node;
				node.input_type = Input_Type::Key;
				node.key_action = Key_Action::Up;
				node.key = (Key) VK_MBUTTON;

				input.nodes.add(node);
			}
			break;

			default:
			{
				return DefWindowProc(h, m, wParam, lParam);
			}
		}

		return false;
	};

	RegisterClass(&wc);



	TracyCZoneN(create_window_zone, "Create window", true);

	int window_width  = 512;
	int window_height = 512;

	int window_x = GetSystemMetrics(SM_CXSCREEN) / 2 - window_width / 2;
	int window_y = GetSystemMetrics(SM_CYSCREEN) / 2 - window_height / 2;

	RECT window_size = {
		window_x,
		window_y,
		window_x + window_width,
		window_y + window_height
	};

	AdjustWindowRect(&window_size, WS_OVERLAPPEDWINDOW, false);

	HWND hwnd = CreateWindowEx(
	    0,                              // Optional window styles.
	    CLASS_NAME,                     // Window class
	    L"Typer",                       // Window text
	    WS_OVERLAPPEDWINDOW,            // Window style

	    // Size and position
	    window_size.left,
		window_size.top,
		window_size.right - window_size.left,
		window_size.bottom - window_size.top,

	    NULL,       // Parent window    
	    NULL,       // Menu
	    hInstance,  // Instance handle
	    NULL        // Additional application data
	    );

	if (hwnd == NULL)
	{
		return 0;
	}

	windows.dc = GetDC(hwnd);
	windows.hwnd = hwnd;
	
	windows.window_dpi = GetDpiForWindow(hwnd);
	windows.window_scaling = float(windows.window_dpi) / 96.0;

	renderer.scaling = windows.window_scaling;

	log(ctx.logger, U"Initial window pixel scaling factor: %", windows.window_scaling);


	TracyCZoneEnd(create_window_zone);

	renderer.init();
	renderer.resize(window_width, window_height);

	input.init();
	typer.init();

	ui.init();


	// Windows will call calbacks and crash the program cause nothing is initialized if we call this earlier.
	ShowWindow(hwnd, SW_SHOWMAXIMIZED);
	SetForegroundWindow(hwnd);


	MSG msg;
	while (1)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			ZoneScopedN("Windows message");

			{
				ZoneScopedN("TranslateMessage");
				TranslateMessage(&msg);
			}

			{
				ZoneScopedN("DispatchMessage");
				
				String str = to_string(msg.message, c_allocator);
				
				ZoneText(str.data, str.length);

				DispatchMessage(&msg);
			}

			switch (msg.message)
			{
				case WM_QUIT:
				{
					fflush(stdout);
					fclose(stdout);
					
					return 0;
				}
			}
		}
		else
		{
			// Clip FPS to the nearest monitor's refresh rate.
			{
				ZoneScopedNC("wait_for_monitor_refresh_rate", 0x00ff00);

				HMONITOR nearest_monitor = MonitorFromWindow(windows.hwnd, MONITOR_DEFAULTTONEAREST);


				MONITORINFOEX monitor_info;
				monitor_info.cbSize = sizeof(monitor_info);

				if (GetMonitorInfoW(nearest_monitor, &monitor_info))
				{
					DEVMODEW dev_mode;
					dev_mode.dmSize = sizeof(dev_mode);

					if (EnumDisplaySettingsW(monitor_info.szDevice, ENUM_CURRENT_SETTINGS, &dev_mode))
					{
						float monitor_frame_time = 1000.0f / float(dev_mode.dmDisplayFrequency);

						float frame_time_ms = frame_time * 1000.0f;
						if (frame_time_ms < monitor_frame_time)
						{
							float diff = monitor_frame_time - frame_time_ms;

							threading.sleep((int) diff);
							//log(ctx.logger, U"%, %, Sleep: %", monitor_frame_time, frame_time_ms, diff);
						}
					}
				}
			}

			typer.do_frame();
		}
	}
}



