#include "Settings.h"

#include "b_lib/Reflection.h"
#include "b_lib/File.h"
#include "b_lib/Tokenizer.h"

#include "Main.h"

bool save_settings()
{
	ZoneScoped;
	
	assert(threading.is_main_thread());

	Unicode_String path = path_concat(frame_allocator, typer.typer_directory, Unicode_String(U"settings.txt"));
	File file = open_file(frame_allocator, path, FILE_WRITE | FILE_CREATE_NEW);

	if (!file.succeeded_to_open())
	{
		return false;
	}

	defer { file.close(); };

	using namespace Reflection;

	{
		Struct_Type* settings_type_info = (Struct_Type*) Reflection::type_of<Settings>();
		for (auto iter = settings_type_info->iterate_members(frame_allocator); Struct_Member* member = iter.next();)
		{
			log(ctx.logger, U"Field: %", member->name);

			file.write(":");
			file.write(member->name);
			file.write(" ");
			
			file.write(write_thing(member->type, add_bytes_to_pointer(&settings, member->offset), frame_allocator));
		
			file.write("\n");
		}
	}

	return true;
}

bool load_settings()
{
	ZoneScoped;

	assert(threading.is_main_thread());

	Unicode_String path = path_concat(frame_allocator, typer.typer_directory, Unicode_String(U"settings.txt"));
	File file = open_file(frame_allocator, path, FILE_READ);

	if (!file.succeeded_to_open())
	{
		return false;
	}

	defer{ file.close(); };

	using namespace Reflection;

	{
		Struct_Type* settings_type_info = (Struct_Type*) Reflection::type_of<Settings>();

		// @MemoryLeak: nothing is properly freed here.
		Tokenizer tokenizer = tokenize(&file, frame_allocator);
		tokenizer.key_characters.add(':');


		String token;
		while (true)
		{
			if (tokenizer.peek_token().is_empty()) break;

			if (!tokenizer.expect_token(":"))
			{
				log(ctx.logger, U"Expected ':' at line: %", tokenizer.line);
				return false;
			}

			token = tokenizer.peek_token();

			Struct_Member* found_member = NULL;
			for (auto iter = settings_type_info->iterate_members(frame_allocator); Struct_Member* member = iter.next();)
			{
				if (member->name == token)
				{
					found_member = member;
					break;
				}
			}

			if (!found_member)
			{
				log(ctx.logger, U"Field '%' wasn't found", token);
				return false;
			}

			if (!read_thing(&tokenizer, found_member->type, add_bytes_to_pointer(&settings, found_member->offset), frame_allocator, c_allocator))
			{
				log(ctx.logger, U"Failed to parse field '%'", found_member->name);
				return false;
			}
		}
	}

	// Validating loaded settings.
	{
		if (!typer.find_font(settings.text_font_face))
		{
			log(ctx.logger, U"Font with name '%' declared in settings.txt wasn't found", settings.text_font_face);

			settings.text_font_face = typer.fonts[0]->name;

			log(ctx.logger, U"Using first available font named '%'", settings.text_font_face);
		}

		int max_size = 100;
		int min_size = 2;
		int default_size = 14;
		if (settings.text_font_face_size < min_size && settings.text_font_face_size > max_size)
		{
			settings.text_font_face_size = default_size;
			log(ctx.logger, U"Expected font size in range [%, %] in settings.txt. Using default size: %", default_size);
		}
	}

	return true;
}