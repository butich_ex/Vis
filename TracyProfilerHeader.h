#pragma once

#pragma push("max")
#undef max


#ifdef TRACY_ENABLE
#define ALLOCATOR_NAMES
#endif


//#define TRACY_ON_DEMAND // This is defined to avoid deadlocking pc when tracy uses >16 gb of memory

#include "tracy/Tracy.hpp"

#include "tracy/TracyC.h"
#pragma pop("max")
