#include "Main.h"

#include "Renderer.h"

#include <xmmintrin.h>
#include <immintrin.h>


void Renderer::init()
{
	ZoneScoped;

	mask_stack = make_array<Mask>(32, c_allocator);
}

void Renderer::clear()
{
	ZoneScoped;

	memset(framebuffer, 0, framebuffer_size);
}

void Renderer::resize(int new_width, int new_height)
{
	ZoneScoped;

	log(ctx.logger, U"Resize to: %, %", new_width, new_height);

	width  = new_width;
	height = new_height;

	framebuffer_size = width * height * channels_count * sizeof(u8);



	mask_buffer_bytes_per_line = align(width, 8); // @Crash: happens if i specify (width / 8) instead of just width.
	
	mask_buffer_size = mask_buffer_bytes_per_line * height * sizeof(u8);

	if (framebuffer)
	{
		c_allocator.free(framebuffer, code_location());
	}

	if (mask_buffer)
	{
		c_allocator.free(mask_buffer, code_location());
	}

	framebuffer = (u8*) c_allocator.alloc(framebuffer_size, code_location());
	assert(is_aligned((u64) framebuffer, 4));

	mask_buffer = (u8*) c_allocator.alloc(mask_buffer_size, code_location());

	recalculate_mask_buffer();
}





void Renderer::set_pixel(int x, int y, rgba rgba)
{
	if (mask_buffer_pixel(x, y) == false) return;

	u8* ptr = framebuffer + (y * width * channels_count) + x * channels_count;

	memcpy(ptr, &rgba, sizeof(rgba.r) * 4);
}

void Renderer::set_pixel_checked(int x, int y, rgba rgba)
{
	if (x < 0 || x >= width || y < 0 || y >= height) return;

	if (mask_buffer_pixel(x, y) == false) return;
	
	u8* ptr = framebuffer + (y * width * channels_count) + x * channels_count;

	memcpy(ptr, &rgba, sizeof(rgba.r) * 4);

	return true;
}

void Renderer::set_pixel_blended_and_checked(int x, int y, rgba rgba)
{
	if (x < 0 || x >= width || y < 0 || y >= height) return;

	if (mask_buffer_pixel(x, y) == false) return;
		
	u8* ptr = framebuffer + (y * width * channels_count) + x * channels_count;

	u32 inverse_alpha = 255 - rgba.a;



	// Is this particular case compiler will generate good assembly already if you enable optimization.
	//   The difference in speed if negligible
#if !TYPER_SIMD && 0
	ptr[0] = ((rgba.a * rgba.r) + (inverse_alpha * ptr[0])) / (255);
	ptr[1] = ((rgba.a * rgba.g) + (inverse_alpha * ptr[1])) / (255);
	ptr[2] = ((rgba.a * rgba.b) + (inverse_alpha * ptr[2])) / (255);
#else

	__m128i pixel_a = _mm_set_epi32(rgba.a, rgba.a, rgba.a, rgba.a);

	__m128i pixel_rgb = _mm_loadu_si32(&rgba);
	pixel_rgb = _mm_cvtepu8_epi32(pixel_rgb);

	__m128i pixel_alpha_multiplied = _mm_mullo_epi32(pixel_a, pixel_rgb);
		

	__m128i inverse_alpha_128 = _mm_set_epi32(inverse_alpha, inverse_alpha, inverse_alpha, inverse_alpha);

	__m128i dst_rgb = _mm_loadu_si32(ptr);
	dst_rgb = _mm_cvtepu8_epi32(dst_rgb);

	__m128i dst_inverse_alpha_multiplied = _mm_mullo_epi32(inverse_alpha_128, dst_rgb);

	__m128i rgb_added = _mm_add_epi32(pixel_alpha_multiplied, dst_inverse_alpha_multiplied);

	__m128i rgb_normalized = _mm_srli_epi32(rgb_added, 8); // divide by 256

	__m128i packed_result = _mm_packs_epi32(rgb_normalized, rgb_normalized);
	packed_result = _mm_packus_epi16(packed_result, packed_result);
	
	_mm_storeu_si32(ptr, packed_result);

#endif
	
	ptr[3] = clamp(0, 255, rgba.a + ptr[3]);
}

void Renderer::recalculate_mask_buffer()
{
	ZoneScopedC(0xff0000);

	if (!redraw_current_frame) return;


	{
		ZoneScopedNC("fill mask buffer with 0xffffffff", 0xff0000);
		memset(mask_buffer, 0xffffffff, mask_buffer_size);
	}


	auto draw_rect_on_mask_buffer = [&](Rect rect, s32 value)
	{
		ZoneScopedNC("draw_rect_on_mask_buffer", 0xff0000);

		
#if TYPER_SIMD && 0
		__m128i fill_value = value ? _mm_set_epi64x(s64_max, s64_max) : _mm_set_epi64x(0, 0);
#else
		u64 fill_value = value ? u64_max : 0;
#endif

		int rect_left_clamped  = clamp(0, width, rect.x_left);
		int rect_right_clamped = clamp(0, width, rect.x_right);

		int width = rect_right_clamped - rect_left_clamped;

		for (int y = clamp(0, height, rect.y_bottom); y < clamp(0, height, rect.y_top); y++)
		{
			// The reason this doesn't uses SIMD is that there are no shift instruction for __mm128i taking non-constant variable.
#if TYPER_SIMD && 0
			u8* addr = mask_buffer + (mask_buffer_bytes_per_line) * y + (rect_left_clamped / (8 * 16));

			{
				__m128i addr_value = _mm_loadu_si128((__m128i*) addr);

				__m128i store_mask = _mm_srli_si128(_mm_set_epi64x(s64_max, s64_max), rect_left_clamped % 128);
				__m128i saved_bits = _mm_andnot_si128(store_mask, addr_value);

				__m128i value_to_store = _mm_and_si128(store_mask, fill_value);
			
				_mm_storeu_si128((__m128i*) addr, _mm_or_si128(_mm_and_si128(value_to_store, addr_value), saved_bits));

				addr += 128 / 8;
			}

			int x = rect_left_clamped + (128 - (rect_left_clamped % 128));
			assert(x % 128 == 0);
			while (true)
			{
				if (x + 128 >= rect_right_clamped)
				{
					int shift_amount = 128 - (rect_left_clamped - x);
					__m128i store_mask = _mm_slli_si128(_mm_srli_si128(_mm_set_epi64x(s64_max, s64_max), shift_amount), shift_amount);

					__m128i addr_value = _mm_loadu_si128(__m128i*) addr);
					__m128i saved_bits = _mm_andnot_si128(store_mask, addr_value);

					__m128i value_to_store = _mm_and_si128(store_mask, fill_value);

					_mm_storeu_si128((__m128i*) addr, _mm_or_si128(_mm_and_si128(value_to_store, addr_value), saved_bits));
				}
				else
				{
					_mm_storeu_si128((__m128i*) addr, _mm_and_si128(fill_value, _mm_loadu_si128((__m128i*) addr)));
				}

				addr += 16;
				x += 128;
			}

#else

			u64* addr = ((u64*) (mask_buffer + mask_buffer_bytes_per_line * y)) + (rect_left_clamped / 64);

			{
				u64 store_mask = (u64_max >> ((rect_left_clamped % 64)));

				if (width < 64)
				{
					// @TODO: test this case by passing a small rect.
					int shift_amount = 64 - width;
					store_mask = (store_mask >> shift_amount) << shift_amount;
				}

				u64 saved_bits = *addr & (~store_mask);

				u64 value_to_store = store_mask & fill_value;
				*addr = (value_to_store & *addr) | saved_bits;

				addr += 1;
			}

			if (width <= 64) continue;

			int x = rect_left_clamped + (64 - (rect_left_clamped % 64));
			assert(x % 64 == 0);
			while (true)
			{
				if (x + 64 >= rect_right_clamped)
				{
					int shift_amount = (64 - (rect_right_clamped - x));
					u64 store_mask = u64_max << shift_amount;

					u64 value_to_store = store_mask & fill_value;

					u64 saved_bits = *addr & (~store_mask);
					*addr = (value_to_store & *addr) | saved_bits;
					break;
				}
				else
				{
					*addr = fill_value & *addr;
				}

				addr += 1;
				x += 64;
			}
#endif
		}
	};

	for (Mask mask: mask_stack)
	{
		// This rects will cover whole screen
		//  Values drawn by this rects are depend on whether mask is reversed.

		// This rects can be fucked up, like right_x less that left_x, but draw_rect_on_mask_buffer
		//  will be fine.
		Rect left_rect = Rect(
			0,
			0,
			mask.rect.x_left,
			renderer.height
		);

		Rect bottom_rect = Rect(
			mask.rect.x_left,
			0,
			mask.rect.x_right,
			mask.rect.y_bottom
		);
		
		Rect right_rect = Rect(
			mask.rect.x_right,
			0,
			renderer.width,
			renderer.height
		);

		Rect top_rect = Rect(
			mask.rect.x_left,
			mask.rect.y_top,
			mask.rect.x_right,
			renderer.height
		);



		s32 outside_value;
		s32 inside_value;

		if (mask.inversed)
		{
			outside_value = 0xffffffff;
			inside_value = 0;
		}
		else
		{
			outside_value = 0;
			inside_value  = 0xffffffff;
		}

		draw_rect_on_mask_buffer(left_rect,   outside_value);
		draw_rect_on_mask_buffer(bottom_rect, outside_value);
		draw_rect_on_mask_buffer(right_rect,  outside_value);
		draw_rect_on_mask_buffer(top_rect,    outside_value);

		draw_rect_on_mask_buffer(mask.rect, inside_value);
	}
}

bool Renderer::mask_buffer_pixel(int x, int y)
{
	//assert(x >= 0 && x < width && y >= 0 && y < height);

	// Keep size of read here equal to writes in recalculate_mask_buffer to not break endianness.

	u64* addr = (u64*) (mask_buffer + (y * mask_buffer_bytes_per_line)) + (x / 64);
	u64 value = *addr;

	u64 check_mask = (1ull << 63) >> (x % 64);

	return (value & check_mask);
}



void Renderer::draw_rect(int left_x, int bottom_y, int right_x, int top_y, rgba rgba)
{
	ZoneScoped;

	if (!redraw_current_frame) return;

	left_x = clamp(0, this->width, left_x);
	right_x = clamp(0, this->width, right_x);
	bottom_y = clamp(0, this->height, bottom_y);
	top_y = clamp(0, this->height, top_y);

	int rect_width = right_x - left_x;
	int rect_height = top_y - bottom_y;

	for (int y = bottom_y; y < top_y; y++)
	{
#if 0 && TYPER_SIMD
		// Not sure if this giving any performance,
		//   but it was really nice struggle.
		const int batch_size = 256 / 32;

		int times     = rect_width / batch_size;
		int remainder = rect_width % batch_size;

		u8* ptr = framebuffer + (y * channels_count * this->width) + (left_x * channels_count);
		u8* mask_buffer_line_start = mask_buffer + (y * this->width) + left_x;

		__m256i color4 = _mm256_set_epi8(
			rgba.a, rgba.b, rgba.g, rgba.r,
			rgba.a, rgba.b, rgba.g, rgba.r,
			rgba.a, rgba.b, rgba.g, rgba.r,
			rgba.a, rgba.b, rgba.g, rgba.r,
			rgba.a, rgba.b, rgba.g, rgba.r,
			rgba.a, rgba.b, rgba.g, rgba.r,
			rgba.a, rgba.b, rgba.g, rgba.r,
			rgba.a, rgba.b, rgba.g, rgba.r);

		u8* mask_ptr = mask_buffer_line_start;
		for (int i = 0; i < times; i++)
		{
			u64 mask_value = *((u64*) mask_ptr);
			__m256i mask = _mm256_set1_epi64x(mask_value);
			mask = _mm256_shuffle_epi8(mask, _mm256_set_epi8(
				39, 39, 39, 39,
				38, 38, 38, 38,
				37, 37, 37, 37,
				36, 36, 36, 36,

				3,3,3,3, 2,2,2,2, 1,1,1,1, 0,0,0,0));

			_mm256_maskstore_epi32((int*) ptr, mask, color4);

			ptr += 4 * batch_size;
			mask_ptr += batch_size;
		}

		int remaining_start = times * batch_size;
		for (int i = remaining_start; i < (remaining_start + remainder); i++)
		{
			if (*mask_buffer_pixel(i + left_x, y))
			{
				memcpy(ptr, &rgba, sizeof(rgba));
			}
			ptr += 4;
		}

#else

		for (int x = left_x; x < right_x; x++)
		{
			set_pixel(x, y, rgba);
		}
#endif
	}
}

void Renderer::draw_rect(Rect rect, rgba rgba)
{
	ZoneScoped;

	if (!redraw_current_frame) return;

	draw_rect(rect.x_left, rect.y_bottom, rect.x_right, rect.y_top, rgba);
}


void Renderer::draw_glyph(Glyph* glyph, int x, int y, rgba color)
{
	ZoneScoped;

	if (!redraw_current_frame) return;


	u8* line = ((u8*) glyph->image_buffer) + (glyph->width * (glyph->height - 1));

	for (int glyph_y = 0; glyph_y < glyph->height; glyph_y++)
	{
		for (int glyph_x = 0; glyph_x < glyph->width; glyph_x++)
		{
			u8 pixel = line[glyph_x];

			set_pixel_blended_and_checked(glyph_x + x, glyph_y + y, rgba(color.r, color.g, color.b, pixel * color.a / 255));
		}

		line -= glyph->width;
	}
}

void Renderer::draw_text(Font::Face* face, Unicode_String str, int x, int y, rgba color)
{
	ZoneScoped;

	// :TextRendering: make sure this matches 'measure_text' in Font.h
	int local_x = x;

	for (int i = 0; i < str.length; i++)
	{
		char32_t c = str[i];

		TracyCZoneN(request_glyph_zone, "request_glyph", true);

		Glyph* glyph = face->request_glyph(c);

		TracyCZoneEnd(request_glyph_zone);

		// :GlyphLocalCoords: this code is magic keep it
		draw_glyph(glyph, local_x + glyph->left_offset, y - (glyph->height - glyph->top_offset), color);

		local_x += glyph->advance;
	}
}


void Renderer::draw_line(int x_start, int y_start, int x_end, int y_end, rgba color)
{
	// Stolen from: https://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm
	// The last piece of code in that page.

	int dx = abs(x_end - x_start);
	int sx = x_start < x_end ? 1 : -1;
	int dy = -abs(y_end - y_start);
	int sy = y_start < y_end ? 1 : -1;
	float err = dx + dy;  /* error value e_xy */
	while (true)
	{
		set_pixel_checked(x_start, y_start, color);
		if (x_start == x_end && y_start == y_end) break;

		float e2 = 2 * err;
		
		if (e2 >= dy)
		{
			err += dy; /* e_xy+e_x > 0 */
			x_start += sx;
		}
		if (e2 <= dx) /* e_xy+e_y < 0 */
		{
			err += dx;
			y_start += sy;
		}
	}
}