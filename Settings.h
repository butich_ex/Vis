#pragma once

#include "b_lib/Reflection.h"


struct Settings
{
	Unicode_String text_font_face = U"Consolas";
	int text_font_face_size = 24;

	bool force_monospaced = false;
};
REFLECT(Settings)
	MEMBER(text_font_face);
	MEMBER(text_font_face_size);
	MEMBER(force_monospaced);
REFLECT_END();

inline Settings settings;


bool save_settings();
bool load_settings();