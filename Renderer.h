#pragma once

#include "b_lib/Basic.h"
#include "b_lib/Font.h"


struct Rect
{
	int x_left;
	int y_bottom;
	int x_right;
	int y_top;

	Rect() {};

	Rect(int x_left, int y_bottom, int x_right, int y_top)
	{
		this->x_left   = x_left;
		this->y_bottom = y_bottom;
		this->x_right  = x_right;
		this->y_top    = y_top;
	}

	inline bool is_point_inside(int x, int y)
	{
		return (x >= x_left && x < x_right && y >= y_bottom && y < y_top);
	}

	inline int height()
	{
		return y_top - y_bottom;
	}

	inline int width()
	{
		return x_right - x_left;
	}


	inline int center_x()
	{
		return (x_right + x_left) / 2;
	}

	inline int center_y()
	{
		return (y_top + y_bottom) / 2;
	}

	inline void move(int x, int y)
	{
		x_left += x;
		x_right += x;
		y_bottom += y;
		y_top += y;
	}

	inline Rect moved(int x, int y)
	{
		Rect copy = *this;
		copy.move(x, y);
		return copy;
	}

};
REFLECT(Rect)
	MEMBER(x_left);
	MEMBER(y_bottom);
	MEMBER(x_right);
	MEMBER(y_top);
REFLECT_END();


struct rgb
{
	u8 r;
	u8 g;
	u8 b;

	rgb() {};

	rgb(u8 r, u8 g, u8 b)
	{
		this->r = r;
		this->g = g;
		this->b = b;
	}
};

inline rgb operator*(rgb a, rgb b)
{
	rgb c;
	c.r = (a.r * b.r) / (255);
	c.g = (a.g * b.g) / (255);
	c.b = (a.b * b.b) / (255);

	return c;
}

struct rgba
{
	union
	{
		struct
		{
			u8 r;
			u8 g;
			u8 b;
		};
		rgb rgb_value;
	};
	u8 a;

	rgba() {};

	rgba(u8 r, u8 g, u8 b, u8 a)
	{
		this->r = r;
		this->g = g;
		this->b = b;
		this->a = a;
	}

	rgba(rgb rgb, u8 a)
	{
		r = rgb.r;
		g = rgb.g;
		b = rgb.b;
		this->a = a;
	}
};

inline rgb operator*(rgb color, float a)
{
	return rgb(
		int(color.r * a),
		int(color.g * a),
		int(color.b * a));
}




struct Renderer
{
	int height;
	int width;

	float scaling = 1.0;
	bool use_dpi_scaling = true;


	constexpr static int channels_count = 4;

	u8* framebuffer;
	u64 framebuffer_size;

	u32 mask_buffer_bytes_per_line;
	u8* mask_buffer;
	u64 mask_buffer_size;


	struct Mask
	{
		Rect rect;
		bool inversed;
	};
	Dynamic_Array<Mask> mask_stack;



	void set_pixel(int x, int y, rgba rgba);
	void set_pixel_checked(int x, int y, rgba rgba);
	void set_pixel_blended_and_checked(int x, int y, rgba rgba);

	void recalculate_mask_buffer();
	bool mask_buffer_pixel(int x, int y);


	void draw_rect(int left_x, int bottom_y, int right_x, int top_y, rgba rgba);
	void draw_rect(Rect rect, rgba rgba);

	void draw_glyph(Glyph* glyph, int x, int y, rgba color);
	void draw_text(Font::Face* face, Unicode_String str, int x, int y, rgba color = rgba(255, 255, 255, 255));
	void draw_line(int x_start, int y_start, int x_end, int y_end, rgba color);


	void clear();

	void resize(int new_width, int new_height);


	void init();


	template <typename T>
	inline T scaled(T number)
	{
		return scale(number, scaling);		
	}
};
inline Renderer renderer;