#include "freetype/ft2build.h"
#include FT_FREETYPE_H

struct Glyph
{
	utf32_char character;
	
	Int2 size;
	Int2 bottom_left_atlas_coord;

	int freetype_glyph_index;
	int advance;
	int left_offset;
	int top_offset;

	void* data;
};


struct Font
{
	String name;
	String file_path;

	struct Face
	{
		int size;
		int baseline_offset;
		int line_spacing;

		Array_Map<utf32_char, Glyph> glyphs;

		Glyph* request_glyph(utf32_char character);
		Glyph* request_ascii_glyph(char character);

		Glyph* create_glyph(utf32_char character);

		FT_Face freetype_face;
	};

	Dynamic_Array<Face> faces;

	Face* create_face(int size);
	Face* get_face(int size);
};

struct Font_Storage
{
	Dynamic_Array<Font> fonts;

	FT_Library ft_library = nullptr;

	Font* find(const Unicode_String& name) const;
	Font* load_font(const Unicode_String& file_path, const Unicode_String& name);
};
