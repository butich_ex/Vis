#pragma once

#include "TracyProfilerHeader.h"

#include "b_lib/Basic.h"
#include "b_lib/String.h"
#include "b_lib/Reflection.h"
#include "b_lib/Format_String.h"
#include "b_lib/Log.h"
#include "b_lib/Font.h"
#include "b_lib/Threading.h"
#include "b_lib/Text_Editor.h"
#include "b_lib/Time_Measurer.h"
#include "b_lib/OS_Clipboard.h"


const inline static Unicode_String ui_font_name = U"NotoSansSC-Medium";
const inline static int ui_font_face_size = 24;


#include "Renderer.h"
#include "UI.h"
#include "Typer_Allocators.h"



#include <Windows.h>
#include <windowsx.h>



#include <Rpc.h>
#pragma comment(lib, "Rpcrt4.lib")


inline UUID get_uuid()
{
	UUID uuid;
	UuidCreate(&uuid);
	return uuid;
}






#define TYPER_SIMD 1



inline double frame_time;
inline u64 frame_index = 0;

inline float avg_fps_last = 0.0;
inline float frame_time_accum = 0.0;
inline int   frame_time_accum_count = 0;
inline const int avg_fps_refresh_rate = 25; // frames

inline u64 next_redraw_frame = 0;
inline bool redraw_current_frame = false;



inline Time_Measurer time_to_from_start_to_first_frame_done = create_time_measurer();












template <typename T>
inline T scale(T number, float by)
{
	return T(float(number) * by);
}



inline int lerp(int a, int b, float c)
{
	return a + int(float((b - a)) * c);
}


inline float lerp(float a, float b, float c)
{
	return a + (b - a) * c;
}



// @TODO
/*
	The thing about getch function is that it uses Windows's specific ReadConsole API, so it doesn't see
	  input in stdin.
*/

struct Windows
{
	HDC dc;
	HWND hwnd;

	UINT window_dpi;
	float window_scaling;

	int window_height;
	int window_width;
	bool window_size_changed = false;

	bool has_window_focus = false;
};

inline Windows windows;







enum class Key
{
	Up_Arrow    = VK_UP,
	Down_Arrow  = VK_DOWN,
	Left_Arrow  = VK_LEFT,
	Right_Arrow = VK_RIGHT,

	Any_Shift = VK_SHIFT,
	Any_Control = VK_CONTROL,
	Any_Alt = VK_MENU,

	Backspace = VK_BACK,
	Delete = VK_DELETE,
	Enter = VK_RETURN,
	Escape = VK_ESCAPE,
	Tab = VK_TAB,

	LMB = VK_LBUTTON,
	RMB = VK_RBUTTON,
	MMB = VK_MBUTTON,


	F3 = VK_F3,
	F6 = VK_F6,

	C = 'C',
	V = 'V',
	X = 'X',
	A = 'A',
};
REFLECT(Key)
	ENUM_VALUE(Up_Arrow);
	ENUM_VALUE(Down_Arrow);
	ENUM_VALUE(Left_Arrow);
	ENUM_VALUE(Right_Arrow);
	
	ENUM_VALUE(Any_Shift);
	ENUM_VALUE(Any_Control);

	ENUM_VALUE(Backspace);
	ENUM_VALUE(Delete);
	ENUM_VALUE(Enter);
	ENUM_VALUE(Escape);

	ENUM_VALUE(LMB);
	ENUM_VALUE(RMB);
	ENUM_VALUE(MMB);

	ENUM_VALUE(C);
	ENUM_VALUE(V);
REFLECT_END();


enum class Input_Type
{
	Key,
	Char,
	Mouse_Wheel,
};
enum class Key_Action
{
	Down,
	Up,
	Hold,
};
REFLECT(Key_Action)
	ENUM_VALUE(Down);
	ENUM_VALUE(Up);
	ENUM_VALUE(Hold);
REFLECT_END();

struct Input_Node
{
	Input_Type input_type;

	union
	{
		struct
		{
			Key key;
			Key_Action key_action;
		};
		int mouse_wheel_delta;
		char32_t character;
	};
};


struct Input_Key_State
{
	Key key_code;
	float press_time;

	Key_Action action;

	bool repeated = false;
};
REFLECT(Input_Key_State)
	MEMBER(key_code);
	MEMBER(press_time);
	MEMBER(action);
REFLECT_END();

struct Input
{
	Dynamic_Array<Input_Node> nodes;

	Dynamic_Array<Input_Key_State> pressed_keys;


	int old_mouse_x = 0;
	int old_mouse_y = 0;

	int mouse_x = 0;
	int mouse_y = 0;

	int mouse_x_delta = 0;
	int mouse_y_delta = 0;

	int mouse_wheel_delta = 0;



	void init();

	void update_key_states();


	bool is_key_down(Key key);
	bool is_key_down_or_held(Key key);
	bool is_key_held(Key key);
	bool is_key_up(Key key);

	bool is_key_combo_pressed(Dynamic_Array<Key> keys);
	bool is_key_combo_pressed(Key key_1, Key key_2);
};
inline Input input;




constexpr static int dot_size = 24;
static_assert((dot_size % 2) == 0);

struct Node;

enum class Node_Type
{
	Function,
	Type_Declaration,

	Statement_Block,

	Variable_Value,

	Statement,
};
REFLECT(Node_Type)
	ENUM_VALUE(Function);
	ENUM_VALUE(Type_Declaration);
	
	ENUM_VALUE(Statement_Block);

	ENUM_VALUE(Variable_Value);
	
	ENUM_VALUE(Statement);
REFLECT_END();



struct Struct_Member
{
	Unicode_String name;
	Node* type;
};

enum class Type_Kind
{
	Primitive,
	Struct,
};
REFLECT(Type_Kind)
	ENUM_VALUE(Primitive);
	ENUM_VALUE(Struct);
REFLECT_END();

enum class Primitive_Kind
{
	P_u8,
	P_u16,
	P_u32,
	P_u64,

	P_s8,
	P_s16,
	P_s32,
	P_s64,

	P_f32,
	P_f64,

	P_bool,
};
REFLECT(Primitive_Kind);
	ENUM_VALUE(P_u8);
	ENUM_VALUE(P_u16);
	ENUM_VALUE(P_u32);
	ENUM_VALUE(P_u64);

	ENUM_VALUE(P_s8);
	ENUM_VALUE(P_s16);
	ENUM_VALUE(P_s32);
	ENUM_VALUE(P_s64);

	ENUM_VALUE(P_f32);
	ENUM_VALUE(P_f64);

	ENUM_VALUE(P_bool);
REFLECT_END();

inline Reflection::Primitive_Kind map_primitive_kind_to_reflection_primitive_kind(Primitive_Kind primitive_kind)
{
	switch (primitive_kind)
	{
		case Primitive_Kind::P_u8:
			return Reflection::Primitive_Kind::P_u8;
		case Primitive_Kind::P_u16:
			return Reflection::Primitive_Kind::P_u16;
		case Primitive_Kind::P_u32:
			return Reflection::Primitive_Kind::P_u32;
		case Primitive_Kind::P_u64:
			return Reflection::Primitive_Kind::P_u64;

		case Primitive_Kind::P_s8:
			return Reflection::Primitive_Kind::P_s8;
		case Primitive_Kind::P_s16:
			return Reflection::Primitive_Kind::P_s16;
		case Primitive_Kind::P_s32:
			return Reflection::Primitive_Kind::P_s32;
		case Primitive_Kind::P_s64:
			return Reflection::Primitive_Kind::P_s64;

		case Primitive_Kind::P_f32:
			return Reflection::Primitive_Kind::P_f32;
		case Primitive_Kind::P_f64:
			return Reflection::Primitive_Kind::P_f64;

		case Primitive_Kind::P_bool:
			return Reflection::Primitive_Kind::P_bool;

		default: assert(false);
	}
}





struct Function_Argument
{
	Node* argument_type;
	Unicode_String name;
};


enum class Statement_Type
{
	If,
	Jump,
	Variable,
	Set_Value,

	None,
};
REFLECT(Statement_Type)
	ENUM_FLAGS(false);

	ENUM_VALUE(If);
	ENUM_VALUE(Jump);
	ENUM_VALUE(Variable);

	ENUM_VALUE(None);
REFLECT_END();


enum Node_Flags: u32
{
	NODE_TOP_DECL = 1,
};

struct Node
{
	Unicode_String name;

	Rect rect;

	rgba color;

	Node_Type node_type;

	u32 node_flags = 0;

	UUID uuid;

	Node() {};

	union
	{
		struct // Function
		{
			Node* function_statement_block;
			Dynamic_Array<Function_Argument> function_arguments;
		};
		struct // Type declaration
		{
			Type_Kind type_kind;

			union
			{
				struct // Primitive 
				{
					Primitive_Kind primitive_kind;
				};
				struct // Struct
				{
					Dynamic_Array<Struct_Member> struct_members;
				};
			};
		};
		struct // Statement block
		{
			Dynamic_Array<Node*> sub_statements;
		};

		struct // Variable value
		{
			Node* variable_value_type;

			union
			{
				Reflection::Primitive_Value primitive_value;
			
				Dynamic_Array<Node*> struct_member_values;
			};
		};

		struct // Statement
		{
			Statement_Type statement_type;

			union
			{
				struct // If 
				{
					Node* if_predicate;
					Node* if_body;
				};
				struct // Jump
				{
					Node* jump_target;
				};
				struct // Variable
				{
					Node*          variable_type;
					Unicode_String variable_name;
				};
				struct // Set value
				{
					Node* variable;
					Node* value;
				};
			};
		};
	};



	inline Rect get_variable_type_type_dot()
	{
		return Rect(
			rect.x_left + renderer.scaled(48),
			rect.y_top - renderer.scaled(36) - renderer.scaled(24),
			rect.x_left + renderer.scaled(48) + renderer.scaled(24),
			rect.y_top - renderer.scaled(36));
	}



	inline Rect get_if_predicate_dot()
	{
		return Rect(
			rect.x_left + renderer.scaled(48),
			rect.y_top - renderer.scaled(72) - renderer.scaled(24),
			rect.x_left + renderer.scaled(48) + renderer.scaled(24),
			rect.y_top - renderer.scaled(72));
	}
	inline Rect get_if_body_dot()
	{
		return Rect(
			rect.x_left + renderer.scaled(48) + renderer.scaled(96),
			rect.y_top - renderer.scaled(72) - renderer.scaled(24),
			rect.x_left + renderer.scaled(48) + renderer.scaled(96) + renderer.scaled(24),
			rect.y_top - renderer.scaled(72));
	}


	inline Rect get_jump_target_dot()
	{
		return Rect(
			rect.x_left + renderer.scaled(48),
			rect.y_top - renderer.scaled(72) - renderer.scaled(24),
			rect.x_left + renderer.scaled(48) + renderer.scaled(24),
			rect.y_top - renderer.scaled(72));
	}


	inline Rect get_variable_type_dot()
	{
		return Rect(
			rect.x_left + renderer.scaled(48),
			rect.y_top - renderer.scaled(72) - renderer.scaled(24),
			rect.x_left + renderer.scaled(48) + renderer.scaled(24),
			rect.y_top - renderer.scaled(72));
	}




	inline Rect get_node_dot()
	{
		return Rect(rect.x_left + renderer.scaled(4), rect.y_top - renderer.scaled(4) - renderer.scaled(24), rect.x_left + renderer.scaled(4) + renderer.scaled(24), rect.y_top - renderer.scaled(4));
	}
};






struct Variable
{
	Node* type;
	Unicode_String name;
};
struct Vis_Thread
{
	Dynamic_Array<Node*> execution_stack;
	Dynamic_Array<Variable> variables;
};


struct Typer
{
	Dynamic_Array<Font> fonts;

	Time_Measurer time_measurer;

	Font* ui_font;
	Font::Face* ui_font_face;


	Node* dragging_node = NULL;
	UI_ID dragging_node_ui_id = null_ui_id;

	Node* dragging_node_this_frame = NULL;
	UI_ID dragging_node_ui_id_this_frame;


	bool do_show_fps = true;



	Vis_Thread main_thread;

	Dynamic_Array<Node> nodes;

	Node* add_node();

	void draw_node(Node* node, int node_index_top_level, int depth);





	Font* find_font(Unicode_String name);
	Font* load_font(Unicode_String path);

	void init();


	void do_frame();
};
inline Typer typer;



inline File log_file;

inline Arena_Allocator frame_allocator;

