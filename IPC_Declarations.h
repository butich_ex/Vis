#pragma once

#include <stdlib.h>


constexpr unsigned long long ipc_shared_memory_size = 4 * 1024 * 1024;



const char TYPER_IPC_SHARED_MEMORY_NAME[] = "Typer_Ipc_Shared_Memory";
const char TYPER_IPC_MUTEX_NAME[]         = "Typer_Ipc_Mutex";
const char TYPER_IPC_BEGIN_PROCESSING_SEMAPHORE_NAME[]     = "Typer_Ipc_Begin_Processing_Semaphore";
const char TYPER_IPC_PROCESSING_DONE_SEMAPHORE_NAME[]     =  "Typer_Ipc_Processing_Done_Semaphore";


const char TYPER_PROCESS_ID_SENDER_SHARED_MEMORY_NAME[] = "Typer_Ipc_Process_Id_Sender_Shared_Memory";


enum CALL_TYPE : unsigned int
{
	CALL_WRITE_CONSOLE_A,
	CALL_WRITE_CONSOLE_W,
	CALL_NT_WRITE_FILE,
	CALL_WRITE_FILE,
	CALL_WRITE_FILE_EX,

	CALL_CREATE_PROCESS_A,
	CALL_CREATE_PROCESS_W,


	CALL_CREATE_PROCESS,
};




constexpr unsigned long long size_of_max_integer_string = 10;
constexpr unsigned long long size_of_shared_memory_name_buffer = sizeof(TYPER_IPC_SHARED_MEMORY_NAME) + size_of_max_integer_string + 1;
constexpr unsigned long long size_of_mutex_name_buffer = sizeof(TYPER_IPC_MUTEX_NAME) + size_of_max_integer_string + 1;
constexpr unsigned long long size_of_begin_processing_semaphore_name_buffer = sizeof(TYPER_IPC_BEGIN_PROCESSING_SEMAPHORE_NAME) + size_of_max_integer_string + 1;
constexpr unsigned long long size_of_processing_done_semaphore_name_buffer = sizeof(TYPER_IPC_PROCESSING_DONE_SEMAPHORE_NAME) + size_of_max_integer_string + 1;
constexpr unsigned long long size_of_process_id_sender_shared_memory_name_buffer = sizeof(TYPER_PROCESS_ID_SENDER_SHARED_MEMORY_NAME) + size_of_max_integer_string + 1;

inline void get_shared_memory_name(const char name_buffer[size_of_shared_memory_name_buffer], DWORD process_id)
{
	sprintf((char*) name_buffer, "%s%u", (char*) TYPER_IPC_SHARED_MEMORY_NAME, process_id);
}

inline void get_mutex_name(const char name_buffer[size_of_mutex_name_buffer], DWORD process_id)
{
	sprintf((char*) name_buffer, "%s%u", (char*) TYPER_IPC_MUTEX_NAME, process_id);
}

inline void get_begin_processing_semaphore_name(const char name_buffer[size_of_begin_processing_semaphore_name_buffer], DWORD process_id)
{
	sprintf((char*) name_buffer, "%s%u", (char*) TYPER_IPC_BEGIN_PROCESSING_SEMAPHORE_NAME, process_id);

}

inline void get_processing_done_semaphore_name(const char name_buffer[size_of_processing_done_semaphore_name_buffer], DWORD process_id)
{
	sprintf((char*) name_buffer, "%s%u", (char*) TYPER_IPC_PROCESSING_DONE_SEMAPHORE_NAME, process_id);
}

inline void get_process_id_sender_shared_memory_name(const char name_buffer[size_of_process_id_sender_shared_memory_name_buffer], DWORD process_id)
{
	sprintf((char*) name_buffer, "%s%u", (char*) TYPER_PROCESS_ID_SENDER_SHARED_MEMORY_NAME, process_id);
}