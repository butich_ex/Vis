#include "Main.h"

#include "UI.h"

Font::Face* UI::get_font_face()
{
	Font* font = typer.find_font(parameters.text_font_name);
	
	if (!font)
		font = typer.find_font(U"ArialMT");

	assert(font);

	Font::Face* face = font->get_face(renderer.scaled(parameters.text_font_face_size));
	return face;
}


void UI::init()
{
	ui_id_data_array = Array_Map<UI_ID, void*>(32, c_allocator);
	active_mask_stack = make_array<Active_Mask>(32, c_allocator);
	active_mask_stack_states = make_array<Active_Mask_Stack_State>(32, c_allocator);
}

void UI::pre_frame()
{
	current_layer = 0;

	current_hovering_layer               = 0;
	current_hovering_scroll_region_layer = 0;

	current_hovering               = invalid_ui_id;
	current_hovering_scroll_region = invalid_ui_id;
}

void UI::post_frame()
{
	assert(active_mask_stack_states.count == 0);

	active_mask_stack.clear();

	hover_scroll_region = current_hovering_scroll_region;
	hover_scroll_region_layer = current_hovering_scroll_region_layer;

	hover = current_hovering;
	hover_layer = current_hovering_layer;

	down = invalid_ui_id;
	up   = invalid_ui_id;

	if (input.is_key_down(Key::LMB))
	{
		if (current_hovering != invalid_ui_id)
		{
			down    = current_hovering;
			holding = current_hovering;
			SetCapture(windows.hwnd);
		}
		else
		{
			down    = null_ui_id;
			holding = null_ui_id;
		}
	}
	
	if (input.is_key_up(Key::LMB))
	{
		up = holding;
		holding = invalid_ui_id;
		SetCapture(NULL);
	}
}

void UI::im_hovering(UI_ID ui_id)
{
	if (current_layer >= current_hovering_layer)
	{
		current_hovering = ui_id;
		current_hovering_layer = current_layer;
	}
}

void UI::this_scroll_region_is_hovering(UI_ID ui_id)
{
	if (current_layer >= current_hovering_scroll_region_layer)
	{
		current_hovering_scroll_region = ui_id;
		current_hovering_scroll_region_layer = current_layer;
	}
}





bool UI::is_point_inside_active_zone(int x, int y)
{
	for (Active_Mask mask: active_mask_stack)
	{
		if (mask.inversed)
		{
			if (mask.rect.is_point_inside(x, y)) return false;
		}
		else
		{
			if (!mask.rect.is_point_inside(x, y)) return false;
		}
	}

	return true;
}

void UI::set_active_masks_as_renderer_masks()
{
	ZoneScoped;

	renderer.mask_stack.clear();

	for (Active_Mask mask: active_mask_stack)
	{
		renderer.mask_stack.add({
			.rect = mask.rect,
			.inversed = mask.inversed,
		});
	}

	renderer.recalculate_mask_buffer();
}

void UI::save_active_mask_stack_state()
{
	ZoneScoped;

	Active_Mask_Stack_State state;
	state.saved_mask_stack = active_mask_stack.copy_with(frame_allocator);

	active_mask_stack_states.add(state);
}

void UI::restore_active_mask_stack_state()
{
	ZoneScoped;

	assert(active_mask_stack_states.count);

	Active_Mask_Stack_State saved_state = active_mask_stack_states.pop_last();

	active_mask_stack.clear();
	saved_state.saved_mask_stack.copy_to(&active_mask_stack);
}




void UI::draw_text(int x, int y, Unicode_String text)
{
	ZoneScoped;


	Font::Face* face = get_font_face();

	int text_width = measure_text_width(text, face);

	int draw_x;

	switch (parameters.text_alignment)
	{
		case Text_Alignment::Left:
		{
			draw_x = x;
		}
		break;

		case Text_Alignment::Center:
		{
			draw_x = x - text_width / 2;
		}
		break;

		case Text_Alignment::Right:
		{
			draw_x = x - text_width;
		}
		break;

		default: assert(false);
	}



	int text_y = y;

	if (parameters.center_text_vertically)
	{
		text_y -= face->baseline_offset;
	}

	renderer.draw_text(face, text, draw_x, text_y, parameters.text_color);
}

bool UI::button(Rect rect, rgba color, UI_ID ui_id)
{
	ZoneScoped;

	UI_Button_State* state = (UI_Button_State*) get_ui_item_data(ui_id);

	if (!state)
	{
		state = (UI_Button_State*) c_allocator.alloc(sizeof(*state), code_location());
		ui_id_data_array.put(ui_id, state);
		
		state->darkness = 0.0;
	}


	bool result = false;


	if (down == ui_id)
	{
		state->darkness = 1.0;
		result = true;
	}

	state->darkness = clamp<double>(0, 1, state->darkness - frame_time * 5.0);
		

	if (is_point_inside_active_zone(input.mouse_x, input.mouse_y) && rect.is_point_inside(input.mouse_x, input.mouse_y) && current_layer >= current_hovering_layer)
	{
		im_hovering(ui_id);
	}



	rgba darken_color = color;
	{
		darken_color.rgb_value = darken_color.rgb_value * lerp(1.0f, 0.3f, state->darkness);
	}

	renderer.draw_rect(rect, darken_color);

	return result;
}

bool UI::button(Rect rect, Unicode_String text, rgba color, UI_ID ui_id)
{
	ZoneScoped;

	bool result = button(rect, color, ui_id);

	scoped_set_and_revert(parameters.text_alignment, Text_Alignment::Center);
	scoped_set_and_revert(parameters.center_text_vertically, true);
	

	draw_text(rect.center_x(), rect.center_y(), text); 

	return result;
}

bool UI::checkbox(Rect rect, bool value, UI_ID ui_id)
{
	ZoneScoped;

	UI_Checkbox_State* state = (UI_Checkbox_State*) get_ui_item_data(ui_id);

	if (!state)
	{
		state = (UI_Checkbox_State*) c_allocator.alloc(sizeof(*state), code_location());
		ui_id_data_array.put(ui_id, state);

		state->dummy = 0;
	}

	bool result = false;

	if (down == ui_id)
	{
		result = true;
	}

	//state->darkness = clamp<double>(0, 1, state->darkness - frame_time * 5.0);


	if (is_point_inside_active_zone(input.mouse_x, input.mouse_y) && rect.is_point_inside(input.mouse_x, input.mouse_y))
	{
		im_hovering(ui_id);
	}


	if (value)
	{
		renderer.draw_rect(rect, parameters.checkbox_background_color);
	}
	else
	{
		renderer.draw_rect(rect, parameters.checkbox_frame_color); // @Performance: borders just should be calculated properly non filling the whole bunch of useless pixels

		Rect inner_rect = rect;
		int border_size = renderer.scaled(1);
		inner_rect.x_left   += border_size;
		inner_rect.y_bottom += border_size;
		inner_rect.x_right  -= border_size;
		inner_rect.y_top    -= border_size;

		renderer.draw_rect(inner_rect, parameters.checkbox_background_color);
	}


	if (value)
	{
		Glyph* tick_glyph = get_font_face()->request_glyph(U'\x2713');

		renderer.draw_glyph(tick_glyph, rect.center_x() - tick_glyph->width / 2, rect.center_y() - tick_glyph->height / 2, parameters.checkbox_tick_color);
	}

	return result;
}



bool UI::text_editor(Rect rect, Unicode_String text, Unicode_String* out_result, UI_ID ui_id)
{
	ZoneScoped;

	renderer.draw_rect(rect, parameters.text_field_background);

	UI_Text_Editor_State* state = (UI_Text_Editor_State*) get_ui_item_data(ui_id);

	if (!state)
	{
		state = (UI_Text_Editor_State*) c_allocator.alloc(sizeof(*state), code_location());
		ui_id_data_array.put(ui_id, state);

		state->text_scroll_left = 0;
		state->editing = false;
		state->time_from_last_mouse_scroll = 1000.0f; // :CopyPaste. 1k seconds will be enough for everyone
	}

	Font::Face* face = get_font_face();


	bool result = false;

	if (is_point_inside_active_zone(input.mouse_x, input.mouse_y) && rect.is_point_inside(input.mouse_x, input.mouse_y))
	{
		im_hovering(ui_id);
	}


	if (down == ui_id)
	{
		if (!state->editing)
		{
			state->builder = build_string<char32_t>(c_allocator); // @MemoryLeak
			state->editor  = create_text_editor(&state->builder);

			state->builder.append(text);

			int chars_before_mouse = pick_appropriate_cursor_position(text, face, input.old_mouse_x - rect.x_left - state->text_scroll_left - parameters.text_field_margin);

			state->editor.cursor = chars_before_mouse;

			state->editing = true;
		}
		else
		{
			int chars_before_mouse = pick_appropriate_cursor_position(state->builder.get_string(), face, input.old_mouse_x - rect.x_left - state->text_scroll_left - parameters.text_field_margin);
			
			if (input.is_key_down_or_held(Key::Any_Shift))
			{
				state->editor.selection_length -= chars_before_mouse - state->editor.cursor;
			}
			else
			{
				state->editor.selection_length = 0;
			}


			state->editor.cursor = chars_before_mouse;
		}
	}
	else if (down != invalid_ui_id) // Clicked on smth else
	{
		if (state->editing)
		{
			state->builder.free();
			state->text_scroll_left = 0;
			state->time_from_last_mouse_scroll = 1000.0f; // :CopyPaste
			state->editing = false;
		}
	}
	else if (holding == ui_id)
	{
		int mouse_clipped = clamp(rect.x_left + 1, rect.x_right - 1, input.mouse_x); // This +1 -1 is some hack to make scroll speed sane when mouse is outside of rect. I don't know how it works.

		int chars_before_mouse = pick_appropriate_cursor_position(state->builder.get_string(), face, mouse_clipped - rect.x_left - state->text_scroll_left - parameters.text_field_margin);

		int old_cursor = state->editor.cursor;

		state->editor.cursor            = chars_before_mouse;
		state->editor.selection_length -= chars_before_mouse - old_cursor;


		if (input.mouse_x < rect.x_left || input.mouse_x > rect.x_right)
		{
			{
				float speed = 1.0;

				float width_for_speed_1 = renderer.scaling * 48.0f;

				if (input.mouse_x > rect.x_right)
				{
					speed = float(input.mouse_x - rect.x_right) / width_for_speed_1;
				}
				else if (input.mouse_x < rect.x_left)
				{
					speed = float(rect.x_left - input.mouse_x) / width_for_speed_1;
				}

				speed = clamp(0.1f, 5.0f, speed);

				state->time_from_last_mouse_scroll += frame_time * speed;
			}


			if (state->time_from_last_mouse_scroll > parameters.text_field_mouse_scroll_delay_per_character)
			{
				state->time_from_last_mouse_scroll = 0.0;

				if (input.mouse_x < rect.x_left)
				{
					state->editor.advance_selection(-1);
				}
				else if (input.mouse_x > rect.x_right)
				{
					state->editor.advance_selection(1);
				}
			}
		}
	}


	active_mask_stack.add({
		.rect = rect,
		.inversed = false,
	});
	set_active_masks_as_renderer_masks();

	defer {
		active_mask_stack.count -= 1;
		set_active_masks_as_renderer_masks();
	};



	if (state->editing)
	{
		for (Input_Node node: input.nodes)
		{
			if (node.input_type == Input_Type::Key)
			{
				if (node.key_action == Key_Action::Down)
				{
					switch (node.key)
					{
						case Key::Backspace:
						{
							state->editor.delete_before_cursor();
						}
						break;
						case Key::Delete:
						{
							state->editor.delete_after_cursor();
						}
						break;

						case Key::Left_Arrow:
						{
							if (input.is_key_down_or_held(Key::Any_Control))
							{
								state->editor.next_word(-1, input.is_key_down_or_held(Key::Any_Shift));
							}
							else
							{
								if (input.is_key_down_or_held(Key::Any_Shift))
								{
									state->editor.advance_selection(-1);
								}
								else
								{
									state->editor.move_cursor(-1);
								}
							}
						}
						break;
						case Key::Right_Arrow:
						{
							if (input.is_key_down_or_held(Key::Any_Control))
							{
								state->editor.next_word(1, input.is_key_down_or_held(Key::Any_Shift));
							}
							else
							{
								if (input.is_key_down_or_held(Key::Any_Shift))
								{
									state->editor.advance_selection(1);
								}
								else
								{
									state->editor.move_cursor(1);
								}
							}
						}
						break;
						case Key::Enter:
						{
							*out_result = state->builder.get_string();
							state->editing = false;
							state->time_from_last_mouse_scroll = 1000.0f; // :CopyPaste
							result = true;
						}
						break;
					}
				}
			}
			else if (node.input_type == Input_Type::Char)
			{
				state->editor.append_at_cursor(node.character);
			}
		}

		if (input.is_key_combo_pressed(Key::Any_Control, Key::C))
		{
			Unicode_String selection = state->editor.get_selected_string();
			copy_to_os_clipboard(selection, frame_allocator);
		}
		else if (input.is_key_combo_pressed(Key::Any_Control, Key::X))
		{
			Unicode_String selection = state->editor.get_selected_string();
			copy_to_os_clipboard(selection, frame_allocator);
			state->editor.maybe_delete_selection();
		}
		else if (input.is_key_combo_pressed(Key::Any_Control, Key::V))
		{
			Unicode_String clipboard = get_os_clipboard<char32_t>(frame_allocator);
			if (clipboard.length)
			{	
				state->editor.maybe_delete_selection();

				state->builder.append(state->editor.cursor, clipboard);

				state->editor.cursor += clipboard.length;
			}
		}
		else if (input.is_key_combo_pressed(Key::Any_Control, Key::A))
		{
			state->editor.cursor = 0;
			state->editor.selection_length = state->builder.length;
		}


		int cursor_left_text_width = measure_text_width(state->builder.get_string().sliced(0, state->editor.cursor), face);

		if (state->editor.selection_length)
		{
			int selection_start = min(state->editor.cursor, state->editor.cursor + state->editor.selection_length);
			int selecion_end    = max(state->editor.cursor, state->editor.cursor + state->editor.selection_length);

			Unicode_String str = state->builder.get_string();



			Rect selection_rect;

			selection_rect.x_left = rect.x_left + state->text_scroll_left + measure_text_width(str.sliced(0, selection_start), face) + parameters.text_field_margin;

			selection_rect.y_bottom = rect.center_y() - face->size / 2;

			selection_rect.x_right = selection_rect.x_left + measure_text_width(str.sliced(selection_start, abs(state->editor.selection_length)), face);

			selection_rect.y_top   = selection_rect.y_bottom + face->size;

			renderer.draw_rect(selection_rect, parameters.text_selection_background);
		}

		
		int cursor_x_left = cursor_left_text_width + state->text_scroll_left + rect.x_left + parameters.text_field_margin;
		renderer.draw_rect(Rect(cursor_x_left, rect.center_y() - face->size / 2, cursor_x_left + parameters.cursor_width, rect.center_y() + face->size / 2), rgba(255, 255, 255, 255));



		int rect_width = rect.x_right - rect.x_left;

		state->text_scroll_left = clamp(-cursor_left_text_width, -cursor_left_text_width + rect_width - parameters.cursor_width - parameters.text_field_margin, state->text_scroll_left);
	}
	else
	{
		state->text_scroll_left = 0;
	}


	scoped_set_and_revert(parameters.text_alignment, Text_Alignment::Left);
	scoped_set_and_revert(parameters.center_text_vertically, true);

	draw_text(rect.x_left + state->text_scroll_left + parameters.text_field_margin, rect.center_y(), state->editing ? state->builder.get_string() : text);

	return result;
}



bool UI::dropdown(Rect rect, int selected, Dynamic_Array<Unicode_String> options, int* out_selected, UI_ID ui_id)
{
	ZoneScoped;

	assert(selected >= 0 && selected < options.count);

	UI_Dropdown_State* state = (UI_Dropdown_State*) get_ui_item_data(ui_id);

	if (!state)
	{
		state = (UI_Dropdown_State*) c_allocator.alloc(sizeof(*state), code_location());
		ui_id_data_array.put(ui_id, state);

		state->is_selected = false;
	}

	// @TODO: figure out what to do with empty options array

	if (is_point_inside_active_zone(input.mouse_x, input.mouse_y) && rect.is_point_inside(input.mouse_x, input.mouse_y))
	{
		im_hovering(ui_id);
	}



	renderer.draw_rect(rect, parameters.dropdown_background_color);

	scoped_set_and_revert(parameters.text_alignment, Text_Alignment::Left);
	scoped_set_and_revert(parameters.center_text_vertically, true);

	draw_text(rect.x_left + parameters.dropdown_text_margin_left, rect.center_y(), *options[selected]);

	bool result = false;


	if (!state->is_selected)
	{
		if (down == ui_id)
		{
			state->is_selected = true;
		}

		// Draw arrow
		renderer.draw_rect(Rect(
			rect.x_right - parameters.dropdown_arrow_margin_right - parameters.dropdown_arrow_size,
			rect.center_y() - parameters.dropdown_arrow_size / 2,
			rect.x_right - parameters.dropdown_arrow_margin_right,
			rect.center_y() + parameters.dropdown_arrow_size / 2),

			parameters.dropdown_arrow_color);
	}
	else
	{
		if (down == ui_id)
		{
			if (rect.is_point_inside(input.old_mouse_x, input.old_mouse_y))
			{
				state->is_selected = false;
			}
		}

		int all_items_height = parameters.dropdown_item_height * options.count;


		int distance_to_bottom = rect.y_bottom;
		int distance_to_top    = renderer.height - rect.y_top;

		bool going_to_the_sky = false;

		if (all_items_height > distance_to_bottom && distance_to_top > distance_to_bottom)
		{
			going_to_the_sky = true;			
		}

		Rect items_rect;
		if (!going_to_the_sky)
		{
			items_rect = Rect(
				rect.x_left,
				rect.y_bottom - min(distance_to_bottom, all_items_height),
				rect.x_right,
				rect.y_bottom);
		}
		else
		{
			items_rect = Rect(
				rect.x_left,
				rect.y_top,
				rect.x_right,
				rect.y_top + min(distance_to_top, all_items_height));
		}


		UI_ID scroll_region_id = ui_id; 
		scroll_region_id.id += 1;

		scoped_set_and_revert(parameters.scroll_region_background, parameters.dropdown_items_list_background);


		Scroll_Region_Result scroll_region_result = scroll_region(items_rect, all_items_height, rect.width(), false, scroll_region_id);


		// @TODO: scroll to the bottom initially if we are going to the top of the screen.

		if (down != invalid_ui_id)
		{
			if (down != ui_id && down != scroll_region_id)
			{
				state->is_selected = false;
			}
		}

		{


			for (int i = 0; i < options.count; i++)
			{
				Unicode_String option = *options[i];
				
				Rect option_rect;
				if (!going_to_the_sky)
				{
					int option_y_top = rect.y_bottom - parameters.dropdown_item_height * i + scroll_region_result.scroll_from_top;

					option_rect = Rect(rect.x_left, option_y_top - parameters.dropdown_item_height, rect.x_right, option_y_top);
				}
				else
				{
					int option_y_bottom = rect.y_top + parameters.dropdown_item_height * i + scroll_region_result.scroll_from_top - max(0, all_items_height - distance_to_top);
					// - max(0, all_items_height - distance_to_top) is needed because we start to draw items 
					//   from the bottom to top.

					option_rect = Rect(rect.x_left, option_y_bottom, rect.x_right, option_y_bottom + parameters.dropdown_item_height);
				}

				option_rect.x_left  -= scroll_region_result.scroll_from_left;
				option_rect.x_right -= scroll_region_result.scroll_from_left;


				// @TODO: do ellipsis (Use ellipsize rountine. Ideas should be in your mind.)

				bool hovering = false;
				if (hover == ui_id)
				{
					if (option_rect.is_point_inside(input.old_mouse_x, input.old_mouse_y))
					{
						hovering = true;
						renderer.draw_rect(option_rect, i == selected ? parameters.dropdown_item_hover_and_selected_background : parameters.dropdown_item_hover_background);
					}
				}

				if (!hovering && i == selected)
				{
					renderer.draw_rect(option_rect, parameters.dropdown_item_selected_background);
				}

				if (down == ui_id)
				{
					if (option_rect.is_point_inside(input.old_mouse_x, input.old_mouse_y))
					{
						state->is_selected = false;
						*out_selected = i;
						result = true;
					}
				}

				draw_text(option_rect.x_left + parameters.dropdown_item_text_left_margin, option_rect.center_y(), option);

				if (is_point_inside_active_zone(input.mouse_x, input.mouse_y) && option_rect.is_point_inside(input.mouse_x, input.mouse_y))
				{
					im_hovering(ui_id);
				}
			}
		}

		end_scroll_region();



		Rect all_rect = items_rect;
		if (going_to_the_sky)
		{
			all_rect.y_bottom -= rect.height();
		} 
		else
		{
			all_rect.y_top += rect.height();
		}

		active_mask_stack.add({
			.rect = all_rect,
			.inversed = true,
			});

		set_active_masks_as_renderer_masks();
	}

	return result;
}

Scroll_Region_Result UI::scroll_region(Rect rect, int content_height, int content_width, bool show_horizontal_scrollbar, UI_ID ui_id)
{
	ZoneScoped;

	UI_Scroll_Region_State* state = (UI_Scroll_Region_State*) get_ui_item_data(ui_id);

	current_layer += 1;

	if (!state)
	{
		state = (UI_Scroll_Region_State*) c_allocator.alloc(sizeof(*state), code_location());
		ui_id_data_array.put(ui_id, state);

		state->scroll_from_top = 0;
		state->scroll_from_left = 0;

		state->dragging_vertical_scrollgrip = false;
		state->dragging_horizontal_scrollgrip = false;
	}

	renderer.draw_rect(rect, parameters.scroll_region_background);
	
	bool do_show_vertical_scrollbar = content_height > rect.height();


	if ((down != invalid_ui_id && down != ui_id) || up == ui_id)
	{
		state->dragging_vertical_scrollgrip = false;
		state->dragging_horizontal_scrollgrip = false;
	}


	if (is_point_inside_active_zone(input.mouse_x, input.mouse_y) && rect.is_point_inside(input.mouse_x, input.mouse_y))
	{
		this_scroll_region_is_hovering(ui_id);
		im_hovering(ui_id);
	}


	// This rect is rect but without scrollbars
	Rect view_rect = rect;

	if (do_show_vertical_scrollbar)
	{
		view_rect.x_right -= parameters.scrollbar_width;
	}

	bool do_show_horizontal_scrollbar = show_horizontal_scrollbar && (content_width > view_rect.width());

	if (do_show_horizontal_scrollbar)
	{
		view_rect.y_bottom += parameters.scrollbar_width;
	}


	int amount_of_vertical_scroll_movement   = content_height - view_rect.height();
	int amount_of_horizontal_scroll_movement = content_width  - view_rect.width();


	if (do_show_vertical_scrollbar)
	{
		Rect vertical_scrollbar_rect = Rect(rect.x_right - parameters.scrollbar_width, rect.y_bottom, rect.x_right, rect.y_top);

		renderer.draw_rect(vertical_scrollbar_rect, parameters.vertical_scrollbar_background_color);


		float visible_to_overall = float(view_rect.height()) / float(content_height);

		int scrollgrip_height = max((int) (visible_to_overall * rect.height()), scale(parameters.min_scrollgrip_height, renderer.scaling));


		int amount_of_scrollgrip_movement = rect.height() - scrollgrip_height;

		int scrollgrip_offset = (int) lerp(0.0, float(amount_of_scrollgrip_movement), float(state->scroll_from_top) / float(amount_of_vertical_scroll_movement));

		Rect scrollgrip_rect = Rect(rect.x_right - parameters.scrollbar_width, rect.y_top - scrollgrip_offset - scrollgrip_height, rect.x_right, rect.y_top - scrollgrip_offset);


		renderer.draw_rect(scrollgrip_rect, state->dragging_vertical_scrollgrip ? parameters.scrollgrip_color_active : parameters.scrollgrip_color);

		if (down == ui_id)
		{
			if (scrollgrip_rect.is_point_inside(input.old_mouse_x, input.old_mouse_y))
			{
				state->dragging_vertical_scrollgrip = true;
				state->dragging_scrollgrip_offset_top = input.old_mouse_y - scrollgrip_rect.y_top;
			}
			else if (vertical_scrollbar_rect.is_point_inside(input.old_mouse_x, input.old_mouse_y))
			{
				int direction = input.old_mouse_y < scrollgrip_rect.center_y() ? 1 : -1;

				scrollgrip_offset = clamp(0, amount_of_scrollgrip_movement, scrollgrip_offset + direction * scrollgrip_rect.height() / 2);

				state->scroll_from_top = (int) lerp(0.0, float(amount_of_vertical_scroll_movement), float(scrollgrip_offset) / float(amount_of_scrollgrip_movement));
			}
		}

		if (state->dragging_vertical_scrollgrip)
		{
			scrollgrip_offset = clamp(0, amount_of_scrollgrip_movement, (vertical_scrollbar_rect.y_top - input.mouse_y) + state->dragging_scrollgrip_offset_top);

			state->scroll_from_top = (int) lerp(0.0, float(amount_of_vertical_scroll_movement), float(scrollgrip_offset) / float(amount_of_scrollgrip_movement));
		}

		if (hover_scroll_region == ui_id)
		{
			state->scroll_from_top = clamp(0, amount_of_vertical_scroll_movement, state->scroll_from_top + input.mouse_wheel_delta * parameters.scroll_region_mouse_wheel_scroll_speed_pixels);
		}
	}

	if (do_show_horizontal_scrollbar)
	{
		Rect horizontal_scrollbar_rect = Rect(rect.x_left, rect.y_bottom, view_rect.x_right, rect.y_bottom + parameters.scrollbar_width);

		renderer.draw_rect(horizontal_scrollbar_rect, parameters.horizontal_scrollbar_background_color);


		// The reason we use view_rect instead of rect (Using rect in vertical scrollbar calculations) is: 
		//  When we show vertical scrollbar we won't use whole rect's width for 
		//    horizontal scrollbar to not overlap with vertical scrollbar.
		//  Instead we use rect.width - (vertical scrollbar width), which equals to view_rect.width()

		float visible_to_overall = float(view_rect.width()) / float(content_width);

		int scrollgrip_width = max((int) (visible_to_overall * view_rect.width()), scale(parameters.min_scrollgrip_height, renderer.scaling));

		int amount_of_scrollgrip_movement = view_rect.width() - scrollgrip_width;

		int scrollgrip_offset = (int) lerp(0.0, float(amount_of_scrollgrip_movement), float(state->scroll_from_left) / float(amount_of_horizontal_scroll_movement));

		Rect scrollgrip_rect = Rect(rect.x_left + scrollgrip_offset, rect.y_bottom, rect.x_left + scrollgrip_offset + scrollgrip_width, rect.y_bottom + parameters.scrollbar_width);

		renderer.draw_rect(scrollgrip_rect, state->dragging_horizontal_scrollgrip ? parameters.scrollgrip_color_active : parameters.scrollgrip_color);

		if (down == ui_id)
		{
			if (scrollgrip_rect.is_point_inside(input.old_mouse_x, input.old_mouse_y))
			{
				state->dragging_horizontal_scrollgrip = true;
				state->dragging_scrollgrip_offset_left = input.old_mouse_x - scrollgrip_rect.x_left;
			}
			else if (horizontal_scrollbar_rect.is_point_inside(input.old_mouse_x, input.old_mouse_y))
			{
				int direction = input.old_mouse_x < scrollgrip_rect.center_x() ? -1 : 1;

				scrollgrip_offset = clamp(0, amount_of_scrollgrip_movement, scrollgrip_offset + direction * scrollgrip_rect.height());

				state->scroll_from_left = (int) lerp(0.0, float(amount_of_horizontal_scroll_movement), float(scrollgrip_offset) / float(amount_of_scrollgrip_movement));
			}
		}

		if (state->dragging_horizontal_scrollgrip)
		{
			scrollgrip_offset = clamp(0, amount_of_scrollgrip_movement, (input.mouse_x - horizontal_scrollbar_rect.x_left) - state->dragging_scrollgrip_offset_left);

			state->scroll_from_left = (int) lerp(0.0, float(amount_of_horizontal_scroll_movement), float(scrollgrip_offset) / float(amount_of_scrollgrip_movement));
		}
	}

	state->scroll_from_top  = clamp(0, amount_of_vertical_scroll_movement,   state->scroll_from_top);
	state->scroll_from_left = clamp(0, amount_of_horizontal_scroll_movement, state->scroll_from_left);




	Scroll_Region_Result result;

	result.did_show_scrollbar            = do_show_vertical_scrollbar;
	result.did_show_horizontal_scrollbar = do_show_horizontal_scrollbar;

	result.scroll_from_top  = state->scroll_from_top;
	result.scroll_from_left = state->scroll_from_left;


	active_mask_stack.add({
		.rect = view_rect,
		.inversed = false,
	});

	set_active_masks_as_renderer_masks();

	return result;
}

void UI::end_scroll_region()
{
	ZoneScoped;

	assert(active_mask_stack.count);

	current_layer -= 1;

	active_mask_stack.count -= 1;
	set_active_masks_as_renderer_masks();
}