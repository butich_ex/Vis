#pragma once

struct Arena_Allocator: public Allocator
{
	Allocator parent_allocator;
	void* data;
	size_t size;
	size_t occupied;

	inline void reset()
	{
		occupied = 0;
	}
};

inline void create_arena_allocator(Arena_Allocator* result, Allocator parent_allocator, size_t allocator_size)
{
	Arena_Allocator impl;
	impl.parent_allocator = parent_allocator;
	impl.data = parent_allocator.alloc(allocator_size, code_location());
	impl.size = allocator_size;
	impl.occupied = 0;

	impl.flags = ALLOCATOR_HAS_NO_FREE_AND_REALLOC;
	impl.allocator_data = (void*) result;

	impl.proc = [](Allocator_Proc_Mode mode, void* old_data, size_t old_size, size_t size, void* allocator_data, Code_Location code_location) -> void*
	{
		Arena_Allocator* impl = (Arena_Allocator*) allocator_data;

		switch (mode)
		{
			case Allocator_Proc_Mode::Allocate:
			{
				size_t remaining = impl->size - impl->occupied;
				if (size > remaining)
				{
					// We can't resize allocator, because old allocations will become invalid.
					assert_msg(false, "No more space at arena allocator. So we fail");
					return NULL;
				}

				void* result = add_bytes_to_pointer(impl->data, impl->occupied);
				impl->occupied += size;
				return result;
			}
			break;
			case Allocator_Proc_Mode::Free:
			{
				
			}
			break;
			case Allocator_Proc_Mode::Realloc:
			{
				assert_msg(false, "Called realloc on Arena_Allocator");
			}
		}

		return NULL;
	};

	*result = impl;
}