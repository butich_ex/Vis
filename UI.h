#pragma once

#include "Renderer.h"

#include "b_lib/Text_Editor.h"



// Ideas in UI code are from Seeque's UI system. 
struct UI_ID
{
	const char* call_site_file_name;
	u32 call_site_line;

	bool use_uuid;
	union
	{
		u32 id;
		UUID uuid;
	};
	u32 sub_id;


	UI_ID() {};

	constexpr UI_ID(const char* call_site_file_name, u32 call_site_line, u32 id):
		call_site_file_name(call_site_file_name),
		call_site_line(call_site_line),
		use_uuid(false),
		id(id),
		sub_id(0)
	{
	}

	constexpr UI_ID(const char* call_site_file_name, u32 call_site_line, UUID uuid, u32 sub_id):
		call_site_file_name(call_site_file_name),
		call_site_line(call_site_line),
		use_uuid(true),
		uuid(uuid),
		sub_id(sub_id)
	{
	}


	inline bool operator==(const UI_ID other) const
	{
		// assert(!call_site_file_name || !use_uuid);

		return other.call_site_file_name == call_site_file_name
			&& other.call_site_line == call_site_line
			&& other.use_uuid == use_uuid
			&& (use_uuid ? ((other.uuid == uuid) && (other.sub_id == sub_id)) : (other.id == id));
	}

	inline bool operator!=(const UI_ID other) const
	{
		return !(other == *this);
	}
};

#define ui_id( id ) UI_ID(__FILE__, __LINE__, id)

#define ui_id_uuid( uuid , sub_id ) UI_ID(__FILE__, __LINE__, uuid, sub_id )

constexpr UI_ID invalid_ui_id = UI_ID(NULL, 0, UUID {}, 0);
constexpr UI_ID null_ui_id = UI_ID("Bruh", u32_max, UUID{}, 0);


struct UI_Button_State
{
	float darkness;
};

struct UI_Checkbox_State
{
	int dummy;
};

struct UI_Text_Editor_State
{
	String_Builder<char32_t> builder;
	Text_Editor<char32_t>    editor;
	s32 text_scroll_left;
	bool editing;

	float time_from_last_mouse_scroll; 
};

struct UI_Dropdown_State
{
	bool is_selected;
};

struct UI_Scroll_Region_State
{
	int scroll_from_top;
	int scroll_from_left;

	bool dragging_vertical_scrollgrip;
	bool dragging_horizontal_scrollgrip;

	int dragging_scrollgrip_offset_top;
	int dragging_scrollgrip_offset_left;
};



struct Scroll_Region_Result
{
	bool did_show_scrollbar;
	bool did_show_horizontal_scrollbar;

	int scroll_from_top;
	int scroll_from_left;
};



enum class Text_Alignment
{
	Left,
	Center,
	Right,
};



struct Active_Mask
{
	Rect rect;
	bool inversed;
};

struct Active_Mask_Stack_State
{
	Dynamic_Array<Active_Mask> saved_mask_stack;
};

struct UI
{
	struct
	{	
		const rgba default_background_color = rgba(48, 48, 48, 255);


		Unicode_String text_font_name = ui_font_name;
		int text_font_face_size       = ui_font_face_size;


		rgba text_color = rgba(255, 255, 255, 255);
		Text_Alignment text_alignment = Text_Alignment::Center;
		bool center_text_vertically = true;
		
		rgba text_field_background = default_background_color;
		rgba text_selection_background = rgba(120, 120, 120, 255);
		int cursor_width = 2;
		int text_field_margin = 8;
		float text_field_mouse_scroll_delay_per_character = 0.025f;

		int scrollbar_width = 24;
		rgba scrollgrip_color = rgba(10, 255, 10, 255);
		rgba scrollgrip_color_active = rgba(10, 180, 10, 255);
		rgba scroll_region_background = rgba(30, 30, 30, 255);
		int min_scrollgrip_height = 24;

		rgba vertical_scrollbar_background_color = rgba(40, 40, 80, 255);
		rgba horizontal_scrollbar_background_color = rgba(80, 40, 40, 255);

		int scroll_region_mouse_wheel_scroll_speed_pixels = 36;

		rgba checkbox_frame_color = rgba(255, 255, 255, 255);
		rgba checkbox_background_color = default_background_color;
		rgba checkbox_tick_color = rgba(255, 255, 255, 255);


		rgba dropdown_background_color = default_background_color;
		int dropdown_text_margin_left = 8;
		int dropdown_arrow_margin_right = 8;
		int dropdown_arrow_size = 4;
		rgba dropdown_arrow_color = rgba(255, 255, 255, 255);
		int dropdown_item_height = 48;
		int dropdown_item_text_left_margin = 16;
		rgba dropdown_items_list_background = rgba(20, 20, 20, 255);
		rgba dropdown_item_hover_background = rgba(40, 40, 40, 255);
		rgba dropdown_item_selected_background = rgba(0, 60, 0, 255);
		rgba dropdown_item_hover_and_selected_background = rgba(0, 10, 0, 255);

	} parameters; 

	Array_Map<UI_ID, void*> ui_id_data_array;

	Dynamic_Array<Active_Mask> active_mask_stack;
	Dynamic_Array<Active_Mask_Stack_State> active_mask_stack_states;
	// You can use this if you want to temprorarily ignore active mask stack.



	UI_ID down; // This is only for current frame
	UI_ID holding = invalid_ui_id;
	UI_ID up;

	UI_ID hover;
	int   hover_layer;


	UI_ID hover_scroll_region;
	int   hover_scroll_region_layer;


	int current_layer = 0;

	UI_ID current_hovering_scroll_region = invalid_ui_id;
	int   current_hovering_scroll_region_layer = 0;


	UI_ID current_hovering = invalid_ui_id;
	int   current_hovering_layer = 0;



	inline void* get_ui_item_data(UI_ID ui_id)
	{
		void** ptr_to_ptr = (void**) ui_id_data_array.get(ui_id);
		if (ptr_to_ptr) return *ptr_to_ptr;

		return NULL;
	}

	Font::Face* get_font_face();



	void init();
	
	void pre_frame();
	void post_frame();

	void im_hovering(UI_ID ui_id);
	void this_scroll_region_is_hovering(UI_ID ui_id);

	bool is_point_inside_active_zone(int x, int y);
	void set_active_masks_as_renderer_masks();


	void save_active_mask_stack_state();
	void restore_active_mask_stack_state();



	void draw_text(int x, int y, Unicode_String text);


	bool button(Rect rect, Unicode_String text, rgba color, UI_ID ui_id);
	bool button(Rect rect, rgba color, UI_ID ui_id);

	// Returns whether you should negate boolean.
	bool checkbox(Rect rect, bool value, UI_ID ui_id);

	// Make sure that you copy resulted string.
	bool text_editor(Rect rect, Unicode_String text, Unicode_String* out_result, UI_ID ui_id);

	// Uses 2 sub ids
	bool dropdown(Rect rect, int selected, Dynamic_Array<Unicode_String> options, int* out_selected, UI_ID ui_id);

	Scroll_Region_Result scroll_region(Rect rect, int content_height, int content_width, bool show_horizontal_scrollbar, UI_ID ui_id);
	void end_scroll_region();
};
inline UI ui;